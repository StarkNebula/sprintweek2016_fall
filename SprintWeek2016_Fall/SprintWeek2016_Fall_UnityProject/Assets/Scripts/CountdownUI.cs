﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class CountdownUI : MonoBehaviour {

    public Text ui;

    public void DoCountdown(Action action)
    {
        StartCoroutine(Countdown(action));
    }
    private IEnumerator Countdown(Action action)
    {
        ui.text = "";
        yield return new WaitForSeconds(1f);

        ui.text = 3.ToString();
        yield return new WaitForSeconds(1f);

        ui.text = 2.ToString();
        yield return new WaitForSeconds(1f);

        ui.text = 1.ToString();
        yield return new WaitForSeconds(1f);

        action.Invoke();
        ui.text = "GO";
        yield return new WaitForSeconds(1f);
        ui.text = "";
    }
}
