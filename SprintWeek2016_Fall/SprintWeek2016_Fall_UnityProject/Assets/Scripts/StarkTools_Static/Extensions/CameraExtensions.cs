﻿// Created by Raphael "Stark" Tetreault 11/09/2015
// Copyright © 2016 Raphael Tetreault
// Last updated 11/09/2015

namespace UnityEngine
{
    /// <summary>
    /// Extensions for the Unity's Camera class.
    /// </summary>
    public static class CameraExtensions
    {
        /// <summary>
        /// Sets the camera's FOV to the <paramref name="focalLength"/>.
        /// </summary>
        /// <param name="camera">The current camera.</param>
        /// <param name="focalLength">The focal length to set the FOV to.</param>
        /// <param name="screenAspectRatio">The aspect ratio of the screen (camera) used in the conversion.</param>
        public static void SetCameraFocalLength(this Camera camera, float focalLength, float screenAspectRatio)
        {
            camera.fieldOfView = CameraUtility.FocalLengthToFieldOfView(screenAspectRatio, focalLength);
        }
        /// <summary>
        /// Sets the camera's FOV to the <paramref name="focalLength"/>. Uses the screens aspect ratio
        /// for conversion.
        /// </summary>
        /// <param name="camera">The current camera.</param>
        /// <param name="focalLength">The focal length to set the FOV to.</param>
        public static void SetCameraFocalLength(this Camera camera, float focalLength)
        {
            camera.fieldOfView = CameraUtility.FocalLengthToFieldOfView(focalLength);
        }

        /// <summary>
        /// Returns the focal length of the <paramref name="camera"/>.
        /// </summary>
        /// <param name="camera">The current camera.</param>
        public static float GetFieldfOfViewAsFocalLength(this Camera camera)
        {
            return CameraUtility.FieldOfViewToFocalLength(camera.fieldOfView);
        }
    }
}