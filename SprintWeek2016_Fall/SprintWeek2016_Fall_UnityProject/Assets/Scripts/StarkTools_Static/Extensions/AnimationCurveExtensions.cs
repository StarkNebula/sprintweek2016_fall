﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class AnimationCurveExtensions
{
    public static float EvaluateTween(this AnimationCurve animationCurve, float time, float endValue)
    {
        return animationCurve.Evaluate(time) / endValue;
    }


}