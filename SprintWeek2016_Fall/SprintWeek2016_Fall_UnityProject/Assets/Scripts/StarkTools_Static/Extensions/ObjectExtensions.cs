﻿using UnityEngine;

public static class ObjectExtensions
{
    public static void DestroyLog(this Object current, Object obj, string log)
    {
        Debug.Log(log);
        Object.Destroy(obj);
    }
    public static void DestroyLogFormat(this Object current, Object obj, string format, params object[] args)
    {
        current.DestroyLog(obj, string.Format(format, args));
    }

    public static void DestroyLogWarning(this Object current, Object obj, string log)
    {
        Debug.LogWarning(log);
        Object.Destroy(obj);
    }
    public static void DestroyLogWarningFormat(this Object current, Object obj, string format, params object[] args)
    {
        current.DestroyLogWarning(obj, string.Format(format, args));
    }

    public static void DestroyLogError(this Object current, Object obj, string log)
    {
        Debug.LogError(log);
        Object.Destroy(obj);
    }
    public static void DestroyLogFormatError(this Object current, Object obj, string format, params object[] args)
    {
        current.DestroyLogError(obj, string.Format(format, args));
    }
}