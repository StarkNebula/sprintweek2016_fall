﻿// Created by Raphael "Stark" Tetreault 01/09/2016
// Copyright © 2016 Raphael Tetreault
// Last updated 21/09/2016

using System;

namespace UnityEngine
{
    /// <summary>
    /// Extensions for Unity's Component class.
    /// </summary>
    public static class ComponentExtensions
    {
        /// <summary>
        /// Indicates whether or not this Component has a Component type of <typeparam name="T"></typeparam> attached.
        /// </summary>
        /// <typeparam name="T">Type of the Component.</typeparam>
        /// <param name="componentType">The current MonoBehaviour.</param>
        public static bool HasComponent<T>(this Component thisComponent) where T : Component
        {
            return (thisComponent.GetComponent<T>() != null) ? true : false;
        }

        /// <summary>
        /// Attempts to return <paramref name="component"/> type of <paramref name="ComponentT"/>.
        /// Throws error if the component does not exist on <paramref name="thisComponent"/> and
        /// returns null.
        /// </summary>
        /// <typeparam name="T">The current object type of T where T : Component.</typeparam>
        /// <typeparam name="ComponentT">The type of component.</typeparam>
        /// <param name="thisComponent">The current GameObject.</param>
        /// <param name="component">The component to check against. Attempts return only if null.</param>
        /// <param name="getComponentMethod">The GetComponents method to use.</param>
        private static Component SafeSingleComponent<T>(T thisComponent, Type component, Func<Component> getComponentMethod) where T : Component
        {
            Component getComponent = getComponentMethod();
            if (getComponent == null)
            {
                GetComponentErrorMessage(thisComponent, component.GetType());
                return null;
            }
            else
                return getComponent;
        }
        /// <summary>
        /// Attempts to return <paramref name="component"/> type of <paramref name="ComponentT"/>.
        /// Throws error if the component does not exist on <paramref name="thisComponent"/> and
        /// returns null.
        /// </summary>
        /// <typeparam name="T">The current object type of T where T : Component.</typeparam>
        /// <typeparam name="ComponentT">The type of component.</typeparam>
        /// <param name="thisComponent">The current GameObject.</param>
        /// <param name="component">The component to check against. Attempts return only if null.</param>
        /// <param name="getComponentMethod">The GetComponents method to use.</param>
        private static Component[] SafeMultipleComponents<T>(T thisComponent, Type components, Func<Component[]> getComponentsMethod) where T : Component
        {
            Component[] getComponents = getComponentsMethod();
            if (getComponents == null)
            {
                GetComponentErrorMessage(thisComponent, components.GetType());
                return null;
            }
            else
                return getComponents;
        }

        /// <summary>
        /// Attempts to return <paramref name="component"/> type of <paramref name="ComponentT"/> if it is currently null.
        /// Throws error if the component does not exist on <paramref name="thisComponent"/> and
        /// returns null.
        /// </summary>
        /// <typeparam name="T">The current object type of T where T : Component.</typeparam>
        /// <typeparam name="ComponentT">The type of component.</typeparam>
        /// <param name="thisComponent">The current GameObject.</param>
        /// <param name="component">The component to check against. Attempts return only if null.</param>
        /// <param name="getComponentMethod">The GetComponents method to use.</param>
        private static Component IfNullSingleComponent<T>(T thisComponent, Object ifNull, Type component, Func<Component> getComponentMethod, out bool check) where T : Component
        {
            check = component == null;

            if (check)
                return SafeSingleComponent(thisComponent, component, getComponentMethod);
            else
                return null;
        }
        /// <summary>
        /// Attempts to return <paramref name="component"/> type of <paramref name="ComponentT"/> if it is currently null.
        /// Throws error if the component does not exist on <paramref name="thisComponent"/> and
        /// returns null.
        /// </summary>
        /// <typeparam name="T">The current object type of T where T : Component.</typeparam>
        /// <typeparam name="ComponentT">The type of component.</typeparam>
        /// <param name="thisComponent">The current GameObject.</param>
        /// <param name="components">The component to check against. Attempts return only if null.</param>
        /// <param name="getComponentsMethod">The GetComponents method to use.</param>
        private static Component[] IfNullMultipleComponents<T>(T thisComponent, Object IfNull, Type components, Func<Component[]> getComponentsMethod, out bool check) where T : Component
        {
            check = components == null;

            if (check)
                return SafeMultipleComponents(thisComponent, components, getComponentsMethod);
            else
                return null;
        }

        // SAFE GET COMPONENT
        /// <summary>
        /// Attempts GetComponent() to retrieve component type of <typeparamref name="T"/>. Prompts console
        /// error if it fails to get type  and returns null.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <param name="thisComponent">The current Component.</param>
        public static T SafeGetComponent<T>(this Component thisComponent) where T : Component
        {
            return SafeSingleComponent(thisComponent, typeof(T), delegate () { return thisComponent.GetComponent<T>(); }) as T;
        }
        /// <summary>
        /// Attempts GetComponentInParent() to retrieve component type of <typeparamref name="T"/>. Prompts console
        /// error if it fails to get type  and returns null.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <param name="thisComponent">The current Component.</param>
        public static T SafeGetComponentInParent<T>(this Component thisComponent) where T : Component
        {
            return SafeSingleComponent(thisComponent, typeof(T), delegate () { return thisComponent.GetComponentInParent<T>(); }) as T;
        }
        /// <summary>
        /// Attempts GetComponentChildren() to retrieve component type of <typeparamref name="T"/>. Prompts console
        /// error if it fails to get type  and returns null.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <param name="thisComponent">The current Component.</param>
        public static T SafeGetComponentInChildren<T>(this Component thisComponent) where T : Component
        {
            return SafeSingleComponent(thisComponent, typeof(T), delegate () { return thisComponent.GetComponentInChildren<T>(); }) as T;
        }
        /// <summary>
        /// Attempts GetComponents() to retrieve components type of <typeparamref name="T"/>. Prompts console
        /// error if it fails to get type  and returns null.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <param name="thisComponent">The current Component.</param>
        public static T[] SafeGetComponents<T>(this Component thisComponent) where T : Component
        {
            return SafeMultipleComponents(thisComponent, typeof(T), delegate () { return thisComponent.GetComponents<T>(); }) as T[];
        }
        /// <summary>
        /// Attempts GetComponentsInParent() to retrieve components type of <typeparamref name="T"/>. Prompts console
        /// error if it fails to get type  and returns null.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <param name="thisComponent">The current Component.</param>
        public static T[] SafeGetComponentsInParent<T>(this Component thisComponent) where T : Component
        {
            return SafeMultipleComponents(thisComponent, typeof(T), delegate () { return thisComponent.GetComponentsInParent<T>(); }) as T[];
        }
        /// <summary>
        /// Attempts GetComponentsInChildren() to retrieve components type of <typeparamref name="T"/>. Prompts console
        /// error if it fails to get type  and returns null.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <param name="thisComponent">The current Component.</param>
        public static T[] SafeGetComponentsInChildren<T>(this Component thisComponent) where T : Component
        {
            return SafeMultipleComponents(thisComponent, typeof(T), delegate () { return thisComponent.GetComponentsInChildren<T>(); }) as T[];
        }

        // IF NULL GET COMPONENT
        /// <summary>
        /// Attempts GetComponent() to retrieve component type of <typeparamref name="T"/> only if the provided object
        /// <paramref name="ifNull"/> is currently null. Prompts console error if it fails to get type  and returns null.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <param name="getComponent">The current Component.</param>
        /// <param name="ifNull">The object to check if null.</param>
        public static T IfNullGetComponent<T>(this Component getComponent, Object ifNull) where T : Component
        {
            bool check;
            return IfNullSingleComponent(getComponent, ifNull, typeof(T), delegate () { return getComponent.GetComponent<T>(); }, out check) as T;
        }
        /// <summary>
        /// Attempts GetComponentInParent() to retrieve component type of <typeparamref name="T"/> only if the provided object
        /// <paramref name="ifNull"/> is currently null. Prompts console error if it fails to get type  and returns null.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <param name="getComponent">The current Component.</param>
        /// <param name="ifNull">The object to check if null.</param>
        public static T IfNullGetComponentInParent<T>(this Component thisComponent, Object ifNull) where T : Component
        {
            bool check;
            return IfNullSingleComponent(thisComponent, ifNull, typeof(T), delegate () { return thisComponent.GetComponentInParent<T>(); }, out check) as T;
        }
        /// <summary>
        /// Attempts GetComponentInChildren() to retrieve component type of <typeparamref name="T"/> only if the provided object
        /// <paramref name="ifNull"/> is currently null. Prompts console error if it fails to get type  and returns null.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <param name="getComponent">The current Component.</param>
        /// <param name="ifNull">The object to check if null.</param>
        public static T IfNullGetComponentInChildren<T>(this Component thisComponent, Object ifNull) where T : Component
        {
            bool check;
            return IfNullSingleComponent(thisComponent, ifNull, typeof(T), delegate () { return thisComponent.GetComponentInChildren<T>(); }, out check) as T;
        }
        /// <summary>
        /// Attempts GetComponents() to retrieve component type of <typeparamref name="T"/> only if the provided object
        /// <paramref name="ifNull"/> is currently null. Prompts console error if it fails to get type  and returns null.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <param name="getComponent">The current Component.</param>
        /// <param name="ifNull">The object to check if null.</param>
        public static T[] IfNullGetComponents<T>(this Component thisComponent, Object ifNull) where T : Component
        {
            bool check;
            return IfNullMultipleComponents(thisComponent, ifNull, typeof(T), delegate () { return thisComponent.GetComponents<T>(); }, out check) as T[];
        }
        /// <summary>
        /// Attempts GetComponentsInParent() to retrieve component type of <typeparamref name="T"/> only if the provided object
        /// <paramref name="ifNull"/> is currently null. Prompts console error if it fails to get type  and returns null.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <param name="getComponent">The current Component.</param>
        /// <param name="ifNull">The object to check if null.</param>
        public static T[] IfNullGetComponentsInParent<T>(this Component thisComponent, Object ifNull) where T : Component
        {
            bool check;
            return IfNullMultipleComponents(thisComponent, ifNull, typeof(T), delegate () { return thisComponent.GetComponentsInParent<T>(); }, out check) as T[];
        }
        /// <summary>
        /// Attempts GetComponentsInChildren() to retrieve component type of <typeparamref name="T"/> only if the provided object
        /// <paramref name="ifNull"/> is currently null. Prompts console error if it fails to get type and returns null.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <param name="getComponent">The current Component.</param>
        /// <param name="ifNull">The object to check if null.</param>
        public static T[] IfNullGetComponentsInChildren<T>(this Component thisComponent, Object ifNull) where T : Component
        {
            bool check;
            return IfNullMultipleComponents(thisComponent, ifNull, typeof(T), delegate () { return thisComponent.GetComponentsInChildren<T>(); }, out check) as T[];
        }

        // IF NULL GET COMPONENT (WITH OUT BOOL)
        /// <summary>
        /// Attempts GetComponent() to retrieve component type of <typeparamref name="T"/> only if the provided object
        /// <paramref name="ifNull"/> is currently null. Prompts console error if it fails to get type  and returns null.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <param name="getComponent">The current Component.</param>
        /// <param name="ifNull">The object to check if null.</param>
        /// <param name="check">Returns true if <paramref name="getComponent"/> returns null.</param>
        public static T IfNullGetComponent<T>(this Component getComponent, Object ifNull, out bool check) where T : Component
        {
            return IfNullSingleComponent(getComponent, ifNull, typeof(T), delegate () { return getComponent.GetComponent<T>(); }, out check) as T;
        }
        /// <summary>
        /// Attempts GetComponentInParent() to retrieve component type of <typeparamref name="T"/> only if the provided object
        /// <paramref name="ifNull"/> is currently null. Prompts console error if it fails to get type  and returns null.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <param name="getComponent">The current Component.</param>
        /// <param name="ifNull">The object to check if null.</param>
        /// <param name="check">Returns true if <paramref name="getComponent"/> returns null.</param>
        public static T IfNullGetComponentInParent<T>(this Component thisComponent, Object ifNull, out bool check) where T : Component
        {
            return IfNullSingleComponent(thisComponent, ifNull, typeof(T), delegate () { return thisComponent.GetComponentInParent<T>(); }, out check) as T;
        }
        /// <summary>
        /// Attempts GetComponentInChildren() to retrieve component type of <typeparamref name="T"/> only if the provided object
        /// <paramref name="ifNull"/> is currently null. Prompts console error if it fails to get type  and returns null.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <param name="getComponent">The current Component.</param>
        /// <param name="ifNull">The object to check if null.</param>
        /// <param name="check">Returns true if <paramref name="getComponent"/> returns null.</param>
        public static T IfNullGetComponentInChildren<T>(this Component thisComponent, Object ifNull, out bool check) where T : Component
        {
            return IfNullSingleComponent(thisComponent, ifNull, typeof(T), delegate () { return thisComponent.GetComponentInChildren<T>(); }, out check) as T;
        }
        /// <summary>
        /// Attempts GetComponents() to retrieve component type of <typeparamref name="T"/> only if the provided object
        /// <paramref name="ifNull"/> is currently null. Prompts console error if it fails to get type  and returns null.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <param name="getComponent">The current Component.</param>
        /// <param name="ifNull">The object to check if null.</param>
        /// <param name="check">Returns true if <paramref name="getComponent"/> returns null.</param>
        public static T[] IfNullGetComponents<T>(this Component thisComponent, Object ifNull, out bool check) where T : Component
        {
            return IfNullMultipleComponents(thisComponent, ifNull, typeof(T), delegate () { return thisComponent.GetComponents<T>(); }, out check) as T[];
        }
        /// <summary>
        /// Attempts GetComponentsInParent() to retrieve component type of <typeparamref name="T"/> only if the provided object
        /// <paramref name="ifNull"/> is currently null. Prompts console error if it fails to get type  and returns null.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <param name="getComponent">The current Component.</param>
        /// <param name="ifNull">The object to check if null.</param>
        /// <param name="check">Returns true if <paramref name="getComponent"/> returns null.</param>
        public static T[] IfNullGetComponentsInParent<T>(this Component thisComponent, Object ifNull, out bool check) where T : Component
        {
            return IfNullMultipleComponents(thisComponent, ifNull, typeof(T), delegate () { return thisComponent.GetComponentsInParent<T>(); }, out check) as T[];
        }
        /// <summary>
        /// Attempts GetComponentsInChildren() to retrieve component type of <typeparamref name="T"/> only if the provided object
        /// <paramref name="ifNull"/> is currently null. Prompts console error if it fails to get type and returns null.
        /// </summary>
        /// <typeparam name="T">The type of component to get.</typeparam>
        /// <param name="getComponent">The current Component.</param>
        /// <param name="ifNull">The object to check if null.</param>
        /// <param name="check">Returns true if <paramref name="getComponent"/> returns null.</param>
        public static T[] IfNullGetComponentsInChildren<T>(this Component thisComponent, Object ifNull, out bool check) where T : Component
        {
            return IfNullMultipleComponents(thisComponent, ifNull, typeof(T), delegate () { return thisComponent.GetComponentsInChildren<T>(); }, out check) as T[];
        }


        /// <summary>
        /// The message that is printed if any extension in MonoBehaviourExtensions fails.
        /// </summary>
        /// <param name="gameObject">The GameObject that GetCompoennt is using.</param>
        /// <param name="type">The type of Component it is trying to get.</param>
        private static void GetComponentErrorMessage(Object obj, Type type)
        {
            //Debug.LogError("The component type <b>" + type.ToString() + "</b> does not exist on GameObject <b>" + obj.name + "</b>!");
            Debug.LogError(string.Format("The component type <b>{0}</b> does not exist on GameObject <b>{1}</b> or in object hierarchy!", type.ToString(), obj.name));
        }
    }
}