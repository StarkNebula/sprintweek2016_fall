﻿// Created by Raphael "Stark" Tetreault 23/07/2016
// Copyright © 2016 Raphael Tetreault
// Last updated 23/07/2016

using UnityEngine;
using System.Collections;

/// <summary>
/// Extensions for Unity's Vector2 structure.
/// </summary>
public static class Vector2Extensions
{
    public static Vector2 Mirror(this Vector2 vector2, bool x, bool y)
    {
        return new Vector2(
            vector2.x * ((x) ? -1f : 1f),
            vector2.y * ((y) ? -1f : 1f)
            );
    }

}
