﻿namespace UnityEngine
{
    public static class CollisionExtensions
    {
        public static Vector3 ContactNormalsAverage(this Collision collision)
        {
            Vector3 normalsAverage = new Vector3();

            for (int i = 0; i < collision.contacts.Length; i++)
                normalsAverage += collision.contacts[i].normal / collision.contacts.Length;

            return normalsAverage;
        }
    }
}