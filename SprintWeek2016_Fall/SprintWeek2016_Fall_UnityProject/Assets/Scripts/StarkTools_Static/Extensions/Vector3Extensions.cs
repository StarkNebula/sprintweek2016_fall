﻿// Created by Raphael "Stark" Tetreault 13/04/2016
// Copyright © 2016 Raphael Tetreault
// Last updated 13/04/2016

namespace UnityEngine
{
    /// <summary>
    /// Extensions for Unity's Vector3 structure.
    /// </summary>
    public static class Vector3Extensions
    {
        public static Vector3 MirrorX(this Vector3 vector3)
        {
            return new Vector3(-vector3.x, vector3.y, vector3.z);
        }
        public static Vector3 MirrorY(this Vector3 vector3)
        {
            return new Vector3(vector3.x, -vector3.y, vector3.z);
        }
        public static Vector3 MirrorZ(this Vector3 vector3)
        {
            return new Vector3(vector3.x, vector3.y, -vector3.z);
        }
        public static Vector3 MirrorXY(this Vector3 vector3)
        {
            return new Vector3(-vector3.x, -vector3.y, vector3.z);
        }
        public static Vector3 MirrorXZ(this Vector3 vector3)
        {
            return new Vector3(-vector3.x, vector3.y, -vector3.z);
        }
        public static Vector3 MirrorYZ(this Vector3 vector3)
        {
            return new Vector3(vector3.x, -vector3.y, -vector3.z);
        }
        public static Vector3 IgnoreAxis(this Vector3 vector, bool x, bool y, bool z)
        {
            if (x)
                vector.x = 0f;
            if (y)
                vector.y = 0f;
            if (z)
                vector.z = 0f;
            return vector;
        }

        public static Vector3 SwapXY(this Vector3 v)
        {
            return new Vector3(v.y, v.x, v.z);
        }
        public static Vector3 SwapXZ(this Vector3 v)
        {
            return new Vector3(v.z, v.y, v.x);
        }
        public static Vector3 SwapYZ(this Vector3 v)
        {
            return new Vector3(v.x, v.z, v.y);
        }

    }
}