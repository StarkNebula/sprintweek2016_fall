﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;

// https://docs.unity3d.com/Manual/ExecutionOrder.html

public class UnityUpdateEvents : MonoBehaviour
{
    public static UnityEvent
        start,
        fixedUpdate,
        update,
        onRenderImage,
        onDrawGizmos,
        onApplicationQuit;

    //
    private static bool triggeredEvent = false;
    void Awake()
    {
        if (!triggeredEvent)
        {
            triggeredEvent = true;
        }
        else
        {
            Debug.LogErrorFormat("The is more than one {0} in the scene! Destroying component on {1}!", GetType(), name);
            Destroy(this);
        }
    }
    //

    public static void Start()
    {
        start.Invoke();
    }
    public static void FixedUpdate()
    {
        fixedUpdate.Invoke();
    }
    public static void Update()
    {
        update.Invoke();
    }
    public static void OnRenderImage()
    {
        onRenderImage.Invoke();
    }
    public static void OnDrawGizmos()
    {
        onDrawGizmos.Invoke();
    }
    public static void OnApplicationQuit()
    {
        onApplicationQuit.Invoke();
    }
}