﻿// Quick GUI with EditorUtility

public interface IEditorSortableList
{
    /// <summary>
    /// Inserts item into list.
    /// </summary>
    /// <example><code>
    /// list.Insert(index, value);
    /// </example></code>
    /// <param name="index"></param>
    void Add(int index);
    /// <summary>
    /// Removes item from list.
    /// </summary>
    /// <example><code>
    /// list.RemoveAt(index);
    /// </example></code>
    /// <param name="index"></param>
    void Remove(int index);
    /// <summary>
    /// Moves an item within list.
    /// </summary>
    /// <example><code>
    /// Type type = list[index];
    /// list.Insert(newIndex, type);
    /// list.RemoveAt((index &lt; newIndex) ? index : index + 1);
    /// </example></code>
    /// <param name="index"></param>
    /// <param name="newIndex"></param>
    void MoveIndex(int index, int newIndex);
}