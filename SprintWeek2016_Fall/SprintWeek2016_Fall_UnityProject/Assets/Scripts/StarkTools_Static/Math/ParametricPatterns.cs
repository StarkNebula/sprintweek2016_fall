﻿// http://stackoverflow.com/questions/3635564/greatest-common-divisor-from-a-set-of-more-than-2-integers
// http://www.codeproject.com/Articles/76878/Spirograph-Shapes-WPF-Bezier-Shapes-from-Math-Form
// https://lekevicius.com/projects/spiralflow/

using UnityEngine;

public static class ParametricPatterns
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="time"></param>
    /// <param name="r1"></param>
    /// <param name="r2"></param>
    /// <returns>
    /// Normalize = (r1 + (r2*2)) * 2
    /// Speed = (#rot) * 360 * (deltatime / secondsPerCycle)
    /// </returns>
    public static Vector2 Epicycloid(float time, float r1, float r2)
    {
        // k = N/D
        float k = (r1 / r2) + 1;
        //Remove scale factor
        //float normalizeScale = r1 + r2 * 2f;

        return new Vector2(
            (r2 * k * MathX.Cos(time)) - (r2 * MathX.Cos(k * time)),
            (r2 * k * MathX.Sin(time)) - (r2 * MathX.Sin(k * time)));
            /// normalizeScale;
    }
    public static Vector2 Epitrochoid(float time, float r1, float r2, float d)
    {
        float k = (r1 / r2) + 1;
        return new Vector2(
            r2 * (k * MathX.Cos(time) -      Mathf.Pow(2, d) * MathX.Cos(k * time)),
            r2 *  k * MathX.Sin(time) - r2 * Mathf.Pow(2, d) * MathX.Sin(k * time));
    }
    public static Vector2 Hypocycloid(float time, float r1, float r2)
    {
        float k = (r1 / r2) - 1;
        return new Vector2(
            r2 * k * MathX.Cos(time) + r2 * MathX.Cos(k * time),
            r2 * k * MathX.Sin(time) - r2 * MathX.Sin(k * time));
    }
    public static Vector2 Hypotrochoid(float time, float r1, float r2, float d)
    {
        float k = (r1 / r2) - 1;
        return new Vector2(
            r2 * (k * MathX.Cos(time) +      Mathf.Pow(2, d) * MathX.Cos(k * time)),
            r2 *  k * MathX.Sin(time) - r2 * Mathf.Pow(2, d) * MathX.Sin(k * time));
    }

    public static Vector2 FarrisWheel(float time, float w1, float w2, float w3, float f1, float f2, float f3, float p1, float p2, float p3)
    {
        float p = time;
        return new Vector2(
            w1 * MathX.Cos(f1 * time + (p + p1)) +
            w2 * MathX.Cos(f2 * time + (p + p2)) +
            w3 * MathX.Cos(f3 * time + (p + p3)),

            w1 * MathX.Sin(f1 * time + (p + p1)) +
            w2 * MathX.Sin(f2 * time + (p + p2)) +
            w3 * MathX.Sin(f3 * time + (p + p3))
            );
    }
    public static Vector2 Lissajous(float time, float angleDeg, int a, int b)
    {
        return new Vector2(
            MathX.Sin(a * time + angleDeg),
            MathX.Sin(b * time)
            );
    }

    /// <summary>
    /// Rose curve. 
    /// </summary>
    /// <param name="time">Time of cycle in degrees (360).</param>
    /// <param name="r1">Value.</param>
    /// <param name="r2">Denominator.</param>
    /// <remarks>
    /// "if k is odd then k is the number of petals"
    /// "if k is even then k is half the number of petals"
    /// 
    /// <paramref name="r1"/> cannot be 0: Divide by zero error.
    /// </remarks>
    /// 
    /// <seealso cref="https://en.wikipedia.org/wiki/Rose_(mathematics)"/>
    public static Vector2 Rose(float time, int r1, int r2)
    {
        float k = (float)r1 / r2;
        return new Vector2(
            MathX.Cos(k * time) * MathX.Cos(time),
            MathX.Cos(k * time) * MathX.Sin(time)
            );
    }
}