﻿// Resources:
// https://en.wikipedia.org/wiki/Parametric_equation

using UnityEngine;

public static class ParametricEquations
{
    #region 2D
    /// <summary>
    /// 
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="time"></param>
    /// <returns>Make <paramref name="a"/> negative to have clockwise rotation.</returns>
    public static Vector2 Circle(Vector2 scale, float time)
    {
        return new Vector2(
            scale.x * MathX.Cos(time),
            scale.y * MathX.Sin(time));
    }
    /// <summary>
    /// UNTESTED
    /// </summary>
    /// <param name="scale"></param>
    /// <param name="time"></param>
    /// <param name="phi"></param>
    /// <returns></returns>
    public static Vector2 Ellipse(Vector2 scale, float time, float phi)
    {
        return new Vector2(
            /*/ a Cos(t) /*/ (scale.x * MathX.Cos(time) * MathX.Cos(phi)) - (scale.y * MathX.Sin(time) * MathX.Sin(phi)),
            /*/ b Sin(t) /*/ (scale.x * MathX.Cos(time) * MathX.Sin(phi)) - (scale.y * MathX.Sin(time) * MathX.Cos(phi)));
    }

    public static Vector2 Hypotrochoid(float outterRadius, float innerRadius, float distance, float time)
    {
        return Hypotrochoid(outterRadius, innerRadius, distance, time, Vector2.one);
    }
    public static Vector2 Hypotrochoid(float outterRadius, float innerRadius, float distance, float time, Vector2 scale)
    {
        // https://en.wikipedia.org/wiki/Parametric_equation#Hypotrochoid

        return new Vector2(
            scale.x * ((outterRadius - innerRadius) * MathX.Cos(time) - distance * MathX.Cos(((outterRadius - innerRadius) / innerRadius) * time)),
            scale.y * ((outterRadius - innerRadius) * MathX.Sin(time) + distance * MathX.Sin(((outterRadius - innerRadius) / innerRadius) * time)));
    }

    public static Vector2 SophisticatedHypotrochoid(float a, float b, float c, float d, float powX, float powY, float time)
    {
        return SophisticatedHypotrochoid(a, b, c, d, powX, powY, time, Vector2.one);
    }
    public static Vector2 SophisticatedHypotrochoid(float a, float b, float c, float d, float powX, float powY, float time, Vector2 scale)
    {
        return new Vector2(
            scale.x * (MathX.Cos(a * time) - MathX.CosPow(b * time, powX)),
            scale.y * (MathX.Sin(c * time) - MathX.SinPow(d * time, powY)));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    /// <param name="time"></param>
    /// <remarks>https://en.wikipedia.org/wiki/Butterfly_curve_(transcendental)</remarks> 
    public static Vector2 ButterflyCurve(float e, float time)
    {
        return new Vector2(
            MathX.Sin(time) * (Mathf.Pow(e, MathX.Cos(time)) - (2 * MathX.Cos(4 * time)) - MathX.SinPow(time / 12f, 5)),
            MathX.Cos(time) * (Mathf.Pow(e, MathX.Cos(time)) - (2 * MathX.Cos(4 * time)) - MathX.SinPow(time / 12f, 5)));
    }

    /// <summary>
    /// UNTESTED
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="t"></param>
    /// <param name="cycle"></param>
    /// <param name="k"></param>
    /// <returns></returns>
    public static Vector2 LissajousCurve(float a, float b, float t, float cycle, Vector2 k)
    {
        return new Vector2(
            a * MathX.Cos(k.x * t),
            b * MathX.Sin(k.y * t));
    }
    #endregion

    #region 3D
    /// <summary>
    /// 
    /// </summary>
    /// <param name="scale"></param>
    /// <param name="z"></param>
    /// <param name="time"></param>
    /// <returns></returns>
    public static Vector3 Helix(Vector2 scale, float z, float time)
    {
        return new Vector3(
            scale.x * MathX.Cos(time),
            scale.y * MathX.Sin(time),
            z * time);
    }
    public static Vector3 HelixHypotrochoid(Vector2 scale, float z, float powX, float powY, float time)
    {
        return new Vector3(
            scale.x * MathX.CosPow(time, powX),
            scale.y * MathX.SinPow(time, powY),
            z * time);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="outterRadius"></param>
    /// <param name="innerRadius"></param>
    /// <param name="timeZ"></param>
    /// <param name="timeXY"></param>
    /// <returns></returns>
    public static Vector3 Torus(float outterRadius, float innerRadius, float timeXY, float timeZ)
    {
        return new Vector3(
            MathX.Cos(timeXY) * (outterRadius + innerRadius * MathX.Cos(timeZ)),
            MathX.Sin(timeXY) * (outterRadius + innerRadius * MathX.Cos(timeZ)),
            innerRadius * MathX.Sin(timeZ));
    }
    #endregion
}
