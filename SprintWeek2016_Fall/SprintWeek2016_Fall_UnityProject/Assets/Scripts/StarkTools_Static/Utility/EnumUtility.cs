﻿using System;

public static class EnumUtility
{
    /// <summary>
    /// Returns the length of the enumeration.
    /// </summary>
    /// <param name="enumeration">The enumeration which to get the length of items from.</param>
    public static int Length(Type enumeration)
    {
        if (!enumeration.GetType().IsEnum)
            throw new ArgumentException("The type " + enumeration.GetType() + " is not an enumeration!");

        return Enum.GetNames(enumeration.GetType()).Length;
    }
}
