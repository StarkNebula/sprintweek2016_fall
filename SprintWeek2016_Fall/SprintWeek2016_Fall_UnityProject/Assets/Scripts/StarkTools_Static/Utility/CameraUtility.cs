﻿// Created by Raphael "Stark" Tetreault 08/09/2016
// Copyright © 2016 Raphael Tetreault
// Last updated 09/09/2016

namespace UnityEngine
{
    /// <summary>
    /// Utility functions for working with Camera.
    /// </summary>
    public static class CameraUtility
    {
        /// <summary>
        /// Converts <paramref name="fov"/> in focal length (mm).
        /// </summary>
        /// <param name="aspectRatio">The aspect ratio of the camera used.</param>
        /// <param name="fov">The Field of View to convert to Focal Length.</param>
        public static float FieldOfViewToFocalLength(float aspectRatio, float fov)
        {
            // Convert aspect ratio into film diagonal
            float filmDiagonal = Mathf.Atan(aspectRatio);

            return filmDiagonal / Mathf.Tan((fov / 2f) * Mathf.Deg2Rad) / 2f;
        }
        /// <summary>
        /// Converts <paramref name="fov"/> in focal length (mm). Uses current screen aspect ratio
        /// for film diagonal.
        /// </summary>
        /// <param name="fov">The Field of View to convert to Focal Length.</param>
        public static float FieldOfViewToFocalLength(float fov)
        {
            return FieldOfViewToFocalLength(ScreenAspectRatio, fov);
        }

        /// <summary>
        /// Converts <paramref name="focalLength"/> (mm) into field of view (fov).
        /// </summary>
        /// <param name="aspectRatio">The aspect ratio of the camera used.</param>
        /// <param name="focalLength">The focal length to convert to field of view (fov).</param>
        public static float FocalLengthToFieldOfView(float aspectRatio, float focalLength)
        {
            // Convert aspect ratio into film diagonal
            float filmDiagonal = Mathf.Atan(aspectRatio);

            return 2f * Mathf.Atan((filmDiagonal / 2f) / focalLength) * Mathf.Rad2Deg;
        }
        /// <summary>
        /// Converts <paramref name="focalLength"/> (mm) into field of view (fov). Uses current screen
        /// aspect ratio for film diagonal.
        /// </summary>
        /// <param name="focalLength">The focal length to convert to field of view (fov).</param>
        public static float FocalLengthToFieldOfView(float focalLength)
        {
            return FocalLengthToFieldOfView(ScreenAspectRatio, focalLength);
        }

        /// <summary>
        /// Returns the angle between diagonal corners of the screen.
        /// </summary>
        public static float ScreenDiagonalAngle
        {
            get
            {
                return Mathf.Atan((float)Screen.height / Screen.width) * Mathf.Rad2Deg;
            }
        }
        /// <summary>
        /// Returns the aspect ratio of height/width as a float.
        /// </summary>
        public static float ScreenAspectRatio
        {
            get
            {
                return (float)Screen.height / Screen.width;
            }
        }
    }
}