﻿// Created by Raphael "Stark" Tetreault 05/09/2016
// Last updated: 05/09/2016
// Copyright © 2016 Raphael Tetreault

using UnityEngine;
using System.Collections;

public static class ArrayUtility
{
    /// <summary>
    /// Returns an array of type <paramref name="T"/> where every index
    /// is initialized to the value of <paramref name="initialValue"/>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="length"></param>
    /// <param name="initialValue"></param>
    public static T[] NewArray<T>(int length, T initialValue)
    {
        T[] array = new T[length];

        for (int i = 0; i < array.Length; i++)
            array[i] = initialValue;

        return array;
    }

    /// <summary>
    /// Returns an int that is always within the bounds of an array (that is not 0 in length).
    /// This is achieved by underflowing or overflowing the int value based on the array's size.
    /// </summary>
    /// <param name="maxValue">The maximum size.</param>
    /// <param name="index">The index to wrap.</param>
    public static int Wrap(int index, int maxValue)
    {
        if (maxValue == 0)
        {
            Debug.LogError("ArrayUtility.Wrap: maxSize cannot be 0!");
            return 0;
        }

        if (index < 0)
            index = maxValue - (Mathf.Abs(index) % maxValue);

        if (index >= maxValue)
            index = index % maxValue;

        return index;
    }
}
