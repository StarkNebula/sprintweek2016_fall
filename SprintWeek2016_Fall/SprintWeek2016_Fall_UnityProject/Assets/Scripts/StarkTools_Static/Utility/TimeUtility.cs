﻿public static class TimeUtility
{
    /// <summary>
    /// Returns the time in days.
    /// </summary>
    /// <param name="time"></param>
    /// <remarks>1 float is equivalent to 1 second.</remarks>
    public static int Days(float time)
    {
        return (int)time / (60 * 60 * 24);
    }
    /// <summary>
    /// Returns the time in hours.
    /// </summary>
    /// <param name="time"></param>
    /// <remarks>1 float is equivalent to 1 second.</remarks>
    public static int Hours(float time)
    {
        return (int)time / (60 * 60);
    }
    /// <summary>
    /// Returns the time in hours. Ranges from 0-23 hours.
    /// </summary>
    /// <param name="time"></param>
    /// <remarks>1 float is equivalent to 1 second.</remarks>
    public static int HoursInADay(float time)
    {
        return Hours(time) % 24;
    }
    /// <summary>
    /// Returns the time in minutes.
    /// </summary>
    /// <param name="time"></param>
    /// <remarks>1 float is equivalent to 1 second.</remarks>
    public static int Minutes(float time)
    {
        return (int)time / 60;
    }
    /// <summary>
    /// Returns the time in minutes. Ranges from 0-59
    /// </summary>
    /// <param name="time"></param>
    /// <remarks>1 float is equivalent to 1 second.</remarks>
    public static int MinutesInAnHour(float time)
    {
        return Minutes(time) % 60;
    }
    /// <summary>
    /// Returns the time in seconds.
    /// </summary>
    /// <param name="time"></param>
    /// <remarks>1 float is equivalent to 1 second.</remarks>
    public static int Seconds(float time)
    {
        return (int)time % 60;
    }
    /// <summary>
    /// Returns the time in seconds. Ranges from 0-59.
    /// </summary>
    /// <param name="time"></param>
    /// <remarks>1 float is equivalent to 1 second.</remarks>
    public static int SecondsInAMinute(float time)
    {
        return Seconds(time) % 60;
    }
    /// <summary>
    /// Returns the time in milliseconds.
    /// </summary>
    /// <param name="time"></param>
    /// <remarks>1 float is equivalent to 1 second.</remarks>
    public static int Milliseconds(float time)
    {
        return (int)(time * 1000);
    }
    /// <summary>
    /// Returns the time in minutes. Ranges from 0-999.
    /// </summary>
    /// <param name="time"></param>
    /// <remarks>1 float is equivalent to 1 second.</remarks>
    public static int MillisecondsInASecond(float time)
    {
        return Milliseconds(time) % 1000;
    }

    public static string TimeDisplay(float time, bool milliseconds, bool seconds, bool minutes, bool hours, char seperator)
    {
        string str = null;

        if (hours) str += Hours(time);
        if (hours & minutes) str += seperator;
        if (minutes) str += MinutesInAnHour(time).ToString("00");
        if (minutes & seconds) str += seperator;
        if (seconds) str += SecondsInAMinute(time).ToString("00");
        if (seconds & milliseconds) str += seperator;
        if (milliseconds) str += MillisecondsInASecond(time).ToString("000");

        return str;
    }
}