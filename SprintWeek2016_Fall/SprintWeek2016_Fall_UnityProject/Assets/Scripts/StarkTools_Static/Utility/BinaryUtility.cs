﻿// Created by Raphael "Stark" Tetreault 27/06/2016
// Copyright © 2016 Raphael Tetreault
// Last updated 14/07/2016

namespace System.IO
{
    /// <summary>
    /// A suite of utilities designed for manipulating and interpreting binary data.
    /// </summary>
    public static class BinaryUtility
    {
        /// <summary>
        /// Reads bits of a <paramref name="value"/> type. 
        /// </summary>
        /// <param name="value">The value to read bits from.</param>
        /// <param name="bitCount">How many bits to read before masking the of the rest value.</param>
        /// <example><code>
        /// byte b = 186;       // Binary: 10111010
        /// 
        /// // Masks the bits after the 5th bit
        /// b = ReadBits(b, 5); // Binary: XXX11010
        /// 
        /// Console.WriteLine("b is now {0}", b);
        /// // Prints "42"
        /// </code></example>
        /// <remarks>
        /// This is most useful when you need to read bits within a value type. For instance,
        /// you can read nibbles when paired with a bitshift operator.
        /// 
        /// <code>
        /// byte b = 166; // 1010 0110
        /// byte nibble1 = ReadBits(b, 4);
        /// byte nibble2 = (byte)(b >> 4);
        /// nibble2      = ReadBits(b, 4);
        /// 
        /// // nibble1 is 0110
        /// // nibble2 is 1010
        /// </code>
        /// </remarks>
        public static byte ReadBits(byte value, int bitCount)
        {
            return (byte)(value & ((1 << bitCount) - 1));
        }
        /// <summary>
        /// Reads bits of a <paramref name="value"/> type. 
        /// </summary>
        /// <param name="value">The value to read bits from.</param>
        /// <param name="bitCount">How many bits to read before masking the of the rest value.</param>
        /// <example><code>
        /// byte b = 186;       // Binary: 10111010
        /// 
        /// // Masks the bits after the 5th bit
        /// b = ReadBits(b, 5); // Binary: XXX11010
        /// 
        /// Console.WriteLine("b is now {0}", b);
        /// // Prints "42"
        /// </code></example>
        /// <remarks>
        /// This is most useful when you need to read bits within a value type. For instance,
        /// you can read nibbles when paired with a bitshift operator.
        /// 
        /// <code>
        /// byte b = 166; // 1010 0110
        /// byte nibble1 = ReadBits(b, 4);
        /// byte nibble2 = (byte)(b >> 4);
        /// nibble2      = ReadBits(b, 4);
        /// 
        /// // nibble1 is 0110
        /// // nibble2 is 1010
        /// </code>
        /// </remarks>
        public static sbyte ReadBits(sbyte value, int bitCount)
        {
            return (sbyte)(value & ((1 << bitCount) - 1));
        }
        /// <summary>
        /// Reads bits of a <paramref name="value"/> type. 
        /// </summary>
        /// <param name="value">The value to read bits from.</param>
        /// <param name="bitCount">How many bits to read before masking the of the rest value.</param>
        /// <example><code>
        /// byte b = 186;       // Binary: 10111010
        /// 
        /// // Masks the bits after the 5th bit
        /// b = ReadBits(b, 5); // Binary: XXX11010
        /// 
        /// Console.WriteLine("b is now {0}", b);
        /// // Prints "42"
        /// </code></example>
        /// <remarks>
        /// This is most useful when you need to read bits within a value type. For instance,
        /// you can read nibbles when paired with a bitshift operator.
        /// 
        /// <code>
        /// byte b = 166; // 1010 0110
        /// byte nibble1 = ReadBits(b, 4);
        /// byte nibble2 = (byte)(b >> 4);
        /// nibble2      = ReadBits(b, 4);
        /// 
        /// // nibble1 is 0110
        /// // nibble2 is 1010
        /// </code>
        /// </remarks>
        public static short ReadBits(short value, int bitCount)
        {
            return (short)(value & ((1 << bitCount) - 1));
        }
        /// <summary>
        /// Reads bits of a <paramref name="value"/> type. 
        /// </summary>
        /// <param name="value">The value to read bits from.</param>
        /// <param name="bitCount">How many bits to read before masking the of the rest value.</param>
        /// <example><code>
        /// byte b = 186;       // Binary: 10111010
        /// 
        /// // Masks the bits after the 5th bit
        /// b = ReadBits(b, 5); // Binary: XXX11010
        /// 
        /// Console.WriteLine("b is now {0}", b);
        /// // Prints "42"
        /// </code></example>
        /// <remarks>
        /// This is most useful when you need to read bits within a value type. For instance,
        /// you can read nibbles when paired with a bitshift operator.
        /// 
        /// <code>
        /// byte b = 166; // 1010 0110
        /// byte nibble1 = ReadBits(b, 4);
        /// byte nibble2 = (byte)(b >> 4);
        /// nibble2      = ReadBits(b, 4);
        /// 
        /// // nibble1 is 0110
        /// // nibble2 is 1010
        /// </code>
        /// </remarks>
        public static ushort ReadBits(ushort value, int bitCount)
        {
            return (ushort)(value & ((1 << bitCount) - 1));
        }
        /// <summary>
        /// Reads bits of a <paramref name="value"/> type. 
        /// </summary>
        /// <param name="value">The value to read bits from.</param>
        /// <param name="bitCount">How many bits to read before masking the of the rest value.</param>
        /// <example><code>
        /// byte b = 186;       // Binary: 10111010
        /// 
        /// // Masks the bits after the 5th bit
        /// b = ReadBits(b, 5); // Binary: XXX11010
        /// 
        /// Console.WriteLine("b is now {0}", b);
        /// // Prints "42"
        /// </code></example>
        /// <remarks>
        /// This is most useful when you need to read bits within a value type. For instance,
        /// you can read nibbles when paired with a bitshift operator.
        /// 
        /// <code>
        /// byte b = 166; // 1010 0110
        /// byte nibble1 = ReadBits(b, 4);
        /// byte nibble2 = (byte)(b >> 4);
        /// nibble2      = ReadBits(b, 4);
        /// 
        /// // nibble1 is 0110
        /// // nibble2 is 1010
        /// </code>
        /// </remarks>
        public static int ReadBits(int value, int bitCount)
        {
            return (value & ((1 << bitCount) - 1));
        }
        /// <summary>
        /// Reads bits of a <paramref name="value"/> type. 
        /// </summary>
        /// <param name="value">The value to read bits from.</param>
        /// <param name="bitCount">How many bits to read before masking the of the rest value.</param>
        /// <example><code>
        /// byte b = 186;       // Binary: 10111010
        /// 
        /// // Masks the bits after the 5th bit
        /// b = ReadBits(b, 5); // Binary: XXX11010
        /// 
        /// Console.WriteLine("b is now {0}", b);
        /// // Prints "42"
        /// </code></example>
        /// <remarks>
        /// This is most useful when you need to read bits within a value type. For instance,
        /// you can read nibbles when paired with a bitshift operator.
        /// 
        /// <code>
        /// byte b = 166; // 1010 0110
        /// byte nibble1 = ReadBits(b, 4);
        /// byte nibble2 = (byte)(b >> 4);
        /// nibble2      = ReadBits(b, 4);
        /// 
        /// // nibble1 is 0110
        /// // nibble2 is 1010
        /// </code>
        /// </remarks>
        public static uint ReadBits(uint value, int bitCount)
        {
            return (value & ((1U << bitCount) - 1));
        }
        /// <summary>
        /// Reads bits of a <paramref name="value"/> type. 
        /// </summary>
        /// <param name="value">The value to read bits from.</param>
        /// <param name="bitCount">How many bits to read before masking the of the rest value.</param>
        /// <example><code>
        /// byte b = 186;       // Binary: 10111010
        /// 
        /// // Masks the bits after the 5th bit
        /// b = ReadBits(b, 5); // Binary: XXX11010
        /// 
        /// Console.WriteLine("b is now {0}", b);
        /// // Prints "42"
        /// </code></example>
        /// <remarks>
        /// This is most useful when you need to read bits within a value type. For instance,
        /// you can read nibbles when paired with a bitshift operator.
        /// 
        /// <code>
        /// byte b = 166; // 1010 0110
        /// byte nibble1 = ReadBits(b, 4);
        /// byte nibble2 = (byte)(b >> 4);
        /// nibble2      = ReadBits(b, 4);
        /// 
        /// // nibble1 is 0110
        /// // nibble2 is 1010
        /// </code>
        /// </remarks>
        public static long ReadBits(long value, int bitCount)
        {
            return (value & ((1L << bitCount) - 1));
        }
        /// <summary>
        /// Reads bits of a <paramref name="value"/> type. 
        /// </summary>
        /// <param name="value">The value to read bits from.</param>
        /// <param name="bitCount">How many bits to read before masking the of the rest value.</param>
        /// <example><code>
        /// byte b = 186;       // Binary: 10111010
        /// 
        /// // Masks the bits after the 5th bit
        /// b = ReadBits(b, 5); // Binary: XXX11010
        /// 
        /// Console.WriteLine("b is now {0}", b);
        /// // Prints "42"
        /// </code></example>
        /// <remarks>
        /// This is most useful when you need to read bits within a value type. For instance,
        /// you can read nibbles when paired with a bitshift operator.
        /// 
        /// <code>
        /// byte b = 166; // 1010 0110
        /// byte nibble1 = ReadBits(b, 4);
        /// byte nibble2 = (byte)(b >> 4);
        /// nibble2      = ReadBits(b, 4);
        /// 
        /// // nibble1 is 0110
        /// // nibble2 is 1010
        /// </code>
        /// </remarks>
        public static ulong ReadBits(ulong value, int bitCount)
        {
            return (value & ((1UL << bitCount) - 1));
        }

        /// <summary>
        /// Bitshift left &lt;&lt; with a return type of byte.
        /// </summary>
        /// <param name="b">The byte value to bitshift.</param>
        /// <param name="i">How many places to bitshift left by.</param>
        public static byte BitShiftLeft(byte b, int i)
        {
            return (byte)((b << i) & 0xFF);
        }
        /// <summary>
        /// Bitshift right &gt;&gt; with a return type of byte.
        /// </summary>
        /// <param name="b">The byte value to bitshift.</param>
        /// <param name="i">How many places to bitshift right by.</param>
        public static byte BitShiftRight(byte b, int i)
        {
            return (byte)((b >> i) & 0xFF);
        }

        /// <summary>
        /// Reades the bit at <paramref name="index"/> as a bool.
        /// </summary>
        /// <param name="value">The value to read a bit as bool from.</param>
        /// <param name="index">The index at which to read a bit, index 0 to n-1.</param>
        public static bool ReadBitAsBool(byte value, int index)
        {
            return ReadBitAsBool((long)value, index);
        }
        /// <summary>
        /// Reades the bit at <paramref name="index"/> as a bool.
        /// </summary>
        /// <param name="value">The value to read a bit as bool from.</param>
        /// <param name="index">The index at which to read a bit, index 0 to n-1.</param>
        public static bool ReadBitAsBool(short value, int index)
        {
            return ReadBitAsBool((long)value, index);
        }
        /// <summary>
        /// Reades the bit at <paramref name="index"/> as a bool.
        /// </summary>
        /// <param name="value">The value to read a bit as bool from.</param>
        /// <param name="index">The index at which to read a bit, index 0 to n-1.</param>
        public static bool ReadBitAsBool(int value, int index)
        {
            return ReadBitAsBool((long)value, index);
        }
        /// <summary>
        /// Reades the bit at <paramref name="index"/> as a bool.
        /// </summary>
        /// <param name="value">The value to read a bit as bool from.</param>
        /// <param name="index">The index at which to read a bit, index 0 to n-1.</param>
        public static bool ReadBitAsBool(long value, int index)
        {
            value = (value >> index); // Shift bit to front of value
            value = (value & 1);      // Clean off value

            return (value > 0) ? true : false;
        }

        /*/
        public static bool ToggleBit(long value, int index, bool b)
        {
            // Pseudocode
            value.bit[index] = b;
            // Use & (AND) for true  0001
            // Use & (AND) for false 1110
        }
        public static bool FlipBit(long value, int index)
        {
            //Pseudocode
            value.bit[index] = !value.bit[index];
            // Use ^ (XOR)
        }
        //*/
    }
}