﻿// Created by Raphael "Stark" Tetreault 02/10/2016
// Last updated: 02/10/2016
// Copyright © 2016 Raphael Tetreault

using UnityEngine;
using System.Collections;
using System;

public static class CoroutineUtility
{
    /// <summary>
    /// REQUIRES TESTING
    /// </summary>
    /// <param name="duration">Lerp duration.</param>
    /// <param name="values">The values to lerp</param>
    public static IEnumerator CoLerp<T>(float duration, params CoroutineLerp<T>[] values)
    {
        // Time, from 0f to 1f
        float currentTime = 0f;
        Debug.Log("Running...");

        while (currentTime < 1f)
        {
            // Increment time by deltaTime scaled by duration
            currentTime += Time.deltaTime / duration;

            // Set the values using the specified Lerp method
            foreach (CoroutineLerp<T> obj in values)
            {
                obj.value = obj.LerpMethod(obj.startValue, obj.endValue, obj.curve.Evaluate(currentTime));
                //Debug.Log(obj.value.GetType());
            }
             
            foreach (CoroutineLerp<T> var in values)
                var.value = var.LerpMethod(var.startValue, var.endValue, var.curve.Evaluate(currentTime));

            // This crashes Unity, but not the above?
            foreach (CoroutineLerp<T> var in values)
                Debug.Log(var.value);

            yield return new WaitForFixedUpdate();
        }

        // Set to final value
        foreach (CoroutineLerp<T> var in values)
            var.value = var.LerpMethod(var.startValue, var.endValue, var.curve.Evaluate(1f));
    }

    /// <summary>
    /// Delays delegate methods for the specified <paramref name="delay"/>.
    /// </summary>
    /// <param name="delay">The time to delay the <paramref name="methods"/> invoked.</param>
    /// <param name="methods">The methods to invoke after the <paramref name="delay"/>.</param>
    public static IEnumerator CoDelay(float delay, params Action[] methods)
    {
        yield return new WaitForSeconds(delay);

        foreach (Action method in methods)
            method.Invoke();
    }
    public static void Delay(MonoBehaviour mono, float delay, params Action[] methods)
    {
        mono.StartCoroutine(CoDelay(delay, methods));
    }
}

/// <summary>
/// Value type for CoroutineUtility.CoLerp method.
/// </summary>
/// <typeparam name="T">The type of param for the CoLerp method.</typeparam>
public class CoroutineLerp<T>
{
    public T value;
    public T startValue, endValue;
    public AnimationCurve curve;
    public Func<T, T, float, T> LerpMethod;

    public CoroutineLerp(T value, T startValue, T endValue, AnimationCurve curve, Func<T,T,float,T> LerpMethod)
    {
        this.value = value;
        this.startValue = startValue;
        this.endValue = endValue;
        this.curve = curve;
        this.LerpMethod = LerpMethod;
    }
}