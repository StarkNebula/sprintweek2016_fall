﻿namespace UnityEngine
{
    public static class AnimationCurveUtility
    {
        /// <summary>
        /// Automatically interprets time linearly for each passed value in order presented.
        /// </summary>
        /// <param name="values">The Keyframe's value.</param>
        public static AnimationCurve LinearlySpacedCurve(params float[] values)
        {
            Keyframe[] k = new Keyframe[values.Length];

            for (int i = 0; i < values.Length; i++)
            {
                k[i].value = values[i];
                k[i].time = (float)i / (values.ArrayLength());
            }

            return new AnimationCurve(k);
        }

        public static AnimationCurve SmoothTangents(this AnimationCurve animationCurve)
        {
            for (int i = 0; i < animationCurve.keys.Length; i++)
                animationCurve.SmoothTangents(i, 1f);

            return animationCurve;
        }
        public static AnimationCurve EaseSmoothTangents(this AnimationCurve animationCurve)
        {
            for (int i = 1; i < animationCurve.keys.Length - 1; i++)
                animationCurve.SmoothTangents(i, 1f);

            return animationCurve;
        }

        /// <summary>
        /// Ease out curve. [AnimationCurve.EaseInOut(0, 0, 1, 1)]
        /// </summary>
        public static readonly AnimationCurve EaseIn = AnimationCurve.EaseInOut(0, 0, 1, 1);
        /// <summary>
        /// Ease in curve. [AnimationCurve.EaseInOut(1, 1, 0, 0)]
        /// </summary>
        public static readonly AnimationCurve EaseOut = AnimationCurve.EaseInOut(1, 1, 0, 0);
        /// <summary>
        /// Ease In Out curve. Starts at 0, goes to 1, then back to 0.
        /// </summary>
        public static readonly AnimationCurve EaseInOut = LinearlySpacedCurve(0, 1, 0);
        /// <summary>
        /// Ease Out In curve. Starts at 1, goes to 0, then back to 1.
        /// </summary>
        public static readonly AnimationCurve EaseOutIn = LinearlySpacedCurve(1, 0, 1);
    }
}