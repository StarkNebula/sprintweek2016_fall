﻿namespace UnityEngine.UI
{
    /// <summary>
    /// A suite of methods to help place Canvas and UI objects relative to a canvas.
    /// </summary>
    public static class CanvasUtility
    {
        [System.Serializable]
        public enum CanvasRelativePosition
        {
            Center,

            Left,
            Right,
            Top,
            Bottom,

            TopLeft,
            TopRight,
            BottomLeft,
            BottomRight,
        }

        #region POSITIONS RELATIVE TO CANVAS
        /// <summary>
        /// Returns the center position of the current screen.
        /// </summary>
        public static Vector2 ScreenCenter
        {
            get { return new Vector2(Screen.width / 2f, Screen.height / 2f); }
        }
        /// <summary>
        /// Returns the center of the screen if it were placed to the left of the current screen.
        /// </summary>
        public static Vector2 ScreenLeft
        {
            get { return ScreenCenter - new Vector2(Screen.width, 0f); }
        }
        /// <summary>
        /// Returns the center of the screen if it were placed to the right of the current screen.
        /// </summary>
        public static Vector2 ScreenRight
        {
            get { return ScreenCenter + new Vector2(Screen.width, 0f); }
        }
        /// <summary>
        /// Returns the center of the screen if it were placed to the above the current screen.
        /// </summary>
        public static Vector2 ScreenTop
        {
            get { return ScreenCenter + new Vector2(0f, Screen.height); }
        }
        /// <summary>
        /// Returns the center of the screen if it were placed to the below the current screen.
        /// </summary>
        public static Vector2 ScreenBottom
        {
            get { return ScreenCenter - new Vector2(0f, Screen.height); }
        }
        /// <summary>
        /// Returns the center of the screen if it were placed above and to the left of the current screen.
        /// </summary>
        public static Vector2 ScreenTopLeft
        {
            get { return ScreenTop + ScreenLeft; }
        }
        /// <summary>
        /// Returns the center of the screen if it were placed above and to the left of the current screen.
        /// </summary>
        public static Vector2 ScreenTopRight
        {
            get { return ScreenTop + ScreenRight; }
        }
        /// <summary>
        /// Returns the center of the screen if it were placed below and to the left of the current screen.
        /// </summary>
        public static Vector2 ScreenBottomLeft
        {
            get { return ScreenBottom + ScreenLeft; }
        }
        /// <summary>
        /// Returns the center of the screen if it were placed below and to the left of the current screen.
        /// </summary>
        public static Vector2 ScreenBottomRight
        {
            get { return ScreenBottom + ScreenRight; }
        }

        /// <summary>
        /// Returns a Vector2 position used to place a UI object relative to the Canvas.
        /// </summary>
        /// <param name="canvasRelativePosition">The relative position to the canvas.</param>
        public static Vector2 GetCanvasRelativePosition(CanvasRelativePosition canvasRelativePosition)
        {
            switch (canvasRelativePosition)
            {
                case CanvasRelativePosition.Left:
                    return ScreenLeft;
                case CanvasRelativePosition.Right:
                    return ScreenRight;
                case CanvasRelativePosition.Top:
                    return ScreenTop;
                case CanvasRelativePosition.Bottom:
                    return ScreenBottom;

                case CanvasRelativePosition.TopLeft:
                    return ScreenTopLeft;
                case CanvasRelativePosition.TopRight:
                    return ScreenTopRight;
                case CanvasRelativePosition.BottomLeft:
                    return ScreenBottomLeft;
                case CanvasRelativePosition.BottomRight:
                    return ScreenBottomRight;

                default:
                    return ScreenCenter;
            }
        }
        /// <summary>
        /// Returns the opposite Vector2 position of <paramref name="canvasRelativePosition"/> used to place a UI object relative to the Canvas.
        /// </summary>
        /// <param name="canvasRelativePosition">The relative position to the canvas.</param>
        public static Vector2 GetOppositeCanvasRelativePosition(CanvasRelativePosition canvasRelativePosition)
        {
            switch (canvasRelativePosition)
            {
                case CanvasRelativePosition.Left:
                    return ScreenRight;
                case CanvasRelativePosition.Right:
                    return ScreenLeft;
                case CanvasRelativePosition.Top:
                    return ScreenBottom;
                case CanvasRelativePosition.Bottom:
                    return ScreenTop;

                case CanvasRelativePosition.TopLeft:
                    return ScreenBottomRight;
                case CanvasRelativePosition.TopRight:
                    return ScreenBottomLeft;
                case CanvasRelativePosition.BottomLeft:
                    return ScreenTopRight;
                case CanvasRelativePosition.BottomRight:
                    return ScreenTopLeft;

                default:
                    return ScreenCenter;
            }
        }
        #endregion

        #region RECT POSITION RELATIVE TO CANVAS
        /// <summary>
        /// Given the <paramref name="rectTransform"/>, the position left of the <paramref name="canvas"/>
        /// where the <paramref name="rectTranform"/> touches but is not on the <paramref name="canvas"/>. 
        /// </summary>
        /// <param name="canvas">The Canvas to compare to.</param>
        /// <param name="rectTransform">The RectTransform to compare to.</param>
        /// <param name="y">The y position to place the <paramref name="rectTransform"/>.</param>
        public static Vector2 RectLeft(Canvas canvas, RectTransform rectTransform, float y)
        {
            return new Vector2(-rectTransform.rect.width / 2f, y) * canvas.scaleFactor;
        }
        /// <summary>
        /// Given the <paramref name="rectTransform"/>, the position right of the <paramref name="canvas"/>
        /// where the <paramref name="rectTranform"/> touches but is not on the <paramref name="canvas"/>. 
        /// </summary>
        /// <param name="canvas">The Canvas to compare to.</param>
        /// <param name="rectTransform">The RectTransform to compare to.</param>
        /// <param name="y">The y position to place the <paramref name="rectTransform"/>.</param>
        public static Vector2 RectRight(Canvas canvas, RectTransform rectTransform, float y)
        {
            return new Vector2(rectTransform.rect.width / 2f, y) * canvas.scaleFactor + ScreenWidth;
        }
        /// <summary>
        /// Given the <paramref name="rectTransform"/>, the position above the <paramref name="canvas"/>
        /// where the <paramref name="rectTranform"/> touches but is not on the <paramref name="canvas"/>. 
        /// </summary>
        /// <param name="canvas">The Canvas to compare to.</param>
        /// <param name="rectTransform">The RectTransform to compare to.</param>
        /// <param name="x">The x position to place the <paramref name="rectTransform"/>.</param>
        public static Vector2 RectTop(Canvas canvas, RectTransform rectTransform, float x)
        {
            return new Vector2(x, rectTransform.rect.height / 2f) * canvas.scaleFactor + ScreenHeight;
        }
        /// <summary>
        /// Given the <paramref name="rectTransform"/>, the position below the <paramref name="canvas"/>
        /// where the <paramref name="rectTranform"/> touches but is not on the <paramref name="canvas"/>. 
        /// </summary>
        /// <param name="canvas">The Canvas to compare to.</param>
        /// <param name="rectTransform">The RectTransform to compare to.</param>
        /// <param name="x">The x position to place the <paramref name="rectTransform"/>.</param>
        public static Vector2 RectBottom(Canvas canvas, RectTransform rectTransform, float x)
        {
            return new Vector2(x, -rectTransform.rect.height / 2f) * canvas.scaleFactor;
        }
        // Preserves x/y position
        /// <summary>
        /// Given the <paramref name="rectTransform"/>, the position left of the <paramref name="canvas"/>
        /// where the <paramref name="rectTranform"/> touches but is not on the <paramref name="canvas"/>.
        /// The y position of the <paramref name="rectTransfrom"/> is preserved.
        /// </summary>
        /// <param name="canvas">The Canvas to compare to.</param>
        /// <param name="rectTransform">The RectTransform to compare to.</param>
        public static Vector2 RectLeft(Canvas canvas, RectTransform rectTransform)
        {
            return RectLeft(canvas, rectTransform, rectTransform.position.y / canvas.scaleFactor);
        }
        /// <summary>
        /// Given the <paramref name="rectTransform"/>, the position right of the <paramref name="canvas"/>
        /// where the <paramref name="rectTranform"/> touches but is not on the <paramref name="canvas"/>.
        /// The y position of the <paramref name="rectTransfrom"/> is preserved.
        /// </summary>
        /// <param name="canvas">The Canvas to compare to.</param>
        /// <param name="rectTransform">The RectTransform to compare to.</param>
        public static Vector2 RectRight(Canvas canvas, RectTransform rectTransform)
        {
            return RectRight(canvas, rectTransform, rectTransform.position.y / canvas.scaleFactor);
        }
        /// <summary>
        /// Given the <paramref name="rectTransform"/>, the position above the <paramref name="canvas"/>
        /// where the <paramref name="rectTranform"/> touches but is not on the <paramref name="canvas"/>. 
        /// The x position of the <paramref name="rectTransfrom"/> is preserved.
        /// </summary>
        /// <param name="canvas">The Canvas to compare to.</param>
        /// <param name="rectTransform">The RectTransform to compare to.</param>
        public static Vector2 RectTop(Canvas canvas, RectTransform rectTransform)
        {
            return RectTop(canvas, rectTransform, rectTransform.position.x / canvas.scaleFactor);
        }
        /// <summary>
        /// Given the <paramref name="rectTransform"/>, the position below the <paramref name="canvas"/>
        /// where the <paramref name="rectTranform"/> touches but is not on the <paramref name="canvas"/>. 
        /// The x position of the <paramref name="rectTransfrom"/> is preserved.
        /// </summary>
        /// <param name="canvas">The Canvas to compare to.</param>
        /// <param name="rectTransform">The RectTransform to compare to.</param>
        public static Vector2 RectBottom(Canvas canvas, RectTransform rectTransform)
        {
            return RectBottom(canvas, rectTransform, rectTransform.position.x / canvas.scaleFactor);
        }
        // Diagonals
        /// <summary>
        /// Given the <paramref name="rectTransform"/>, the position above to the left of the <paramref name="canvas"/>
        /// where the <paramref name="rectTranform"/> touches but is not on the <paramref name="canvas"/>. 
        /// </summary>
        /// <param name="canvas">The Canvas to compare to.</param>
        /// <param name="rectTransform">The RectTransform to compare to.</param>
        public static Vector2 RectTopLeft(Canvas canvas, RectTransform rectTransform)
        {
            return RectTop(canvas, rectTransform, 0) + RectLeft(canvas, rectTransform, 0);
        }
        /// <summary>
        /// Given the <paramref name="rectTransform"/>, the position above to the right of the <paramref name="canvas"/>
        /// where the <paramref name="rectTranform"/> touches but is not on the <paramref name="canvas"/>. 
        /// </summary>
        /// <param name="canvas">The Canvas to compare to.</param>
        /// <param name="rectTransform">The RectTransform to compare to.</param>
        public static Vector2 RectTopRight(Canvas canvas, RectTransform rectTransform)
        {
            return RectTop(canvas, rectTransform, 0) + RectRight(canvas, rectTransform, 0);
        }
        /// <summary>
        /// Given the <paramref name="rectTransform"/>, the position below to the left of the <paramref name="canvas"/>
        /// where the <paramref name="rectTranform"/> touches but is not on the <paramref name="canvas"/>. 
        /// </summary>
        /// <param name="canvas">The Canvas to compare to.</param>
        /// <param name="rectTransform">The RectTransform to compare to.</param>
        public static Vector2 RectBottomLeft(Canvas canvas, RectTransform rectTransform)
        {
            return RectBottom(canvas, rectTransform, 0) + RectLeft(canvas, rectTransform, 0);
        }
        /// <summary>
        /// Given the <paramref name="rectTransform"/>, the position below to the right of the <paramref name="canvas"/>
        /// where the <paramref name="rectTranform"/> touches but is not on the <paramref name="canvas"/>. 
        /// </summary>
        /// <param name="canvas">The Canvas to compare to.</param>
        /// <param name="rectTransform">The RectTransform to compare to.</param>
        public static Vector2 RectBottomRight(Canvas canvas, RectTransform rectTransform)
        {
            return RectBottom(canvas, rectTransform, 0) + RectRight(canvas, rectTransform, 0);
        }

        /// <summary>
        /// Returns a Vector2 position used to place a RectTransform object relative to the Canvas.
        /// </summary>
        /// <param name="canvasRelativePosition">The relative position to the canvas.</param>
        public static Vector2 GetRectRelativePosition(CanvasRelativePosition canvasRelativePosition, Canvas canvas, RectTransform rectTransform)
        {
            switch (canvasRelativePosition)
            {
                case CanvasRelativePosition.Left:
                    return RectLeft(canvas, rectTransform);
                case CanvasRelativePosition.Right:
                    return RectRight(canvas, rectTransform);
                case CanvasRelativePosition.Top:
                    return RectTop(canvas, rectTransform);
                case CanvasRelativePosition.Bottom:
                    return RectBottom(canvas, rectTransform);

                case CanvasRelativePosition.TopLeft:
                    return RectTopLeft(canvas, rectTransform);
                case CanvasRelativePosition.TopRight:
                    return RectTopRight(canvas, rectTransform);
                case CanvasRelativePosition.BottomLeft:
                    return RectBottomLeft(canvas, rectTransform);
                case CanvasRelativePosition.BottomRight:
                    return RectBottomRight(canvas, rectTransform);

                default:
                    return ScreenCenter;
            }
        }
        /// <summary>
        /// Returns the opposite Vector2 position of <paramref name="canvasRelativePosition"/> used to place a RectTransform object relative to the Canvas.
        /// </summary>
        /// <param name="canvasRelativePosition">The relative position to the canvas.</param>
        public static Vector2 GetOppositeRectRelativePosition(CanvasRelativePosition canvasRelativePosition, Canvas canvas, RectTransform rectTransform)
        {
            switch (canvasRelativePosition)
            {
                case CanvasRelativePosition.Left:
                    return RectRight(canvas, rectTransform);
                case CanvasRelativePosition.Right:
                    return RectLeft(canvas, rectTransform);
                case CanvasRelativePosition.Top:
                    return RectBottom(canvas, rectTransform);
                case CanvasRelativePosition.Bottom:
                    return RectTop(canvas, rectTransform);

                case CanvasRelativePosition.TopLeft:
                    return RectBottomRight(canvas, rectTransform);
                case CanvasRelativePosition.TopRight:
                    return RectBottomLeft(canvas, rectTransform);
                case CanvasRelativePosition.BottomLeft:
                    return RectTopRight(canvas, rectTransform);
                case CanvasRelativePosition.BottomRight:
                    return RectTopLeft(canvas, rectTransform);

                default:
                    return ScreenCenter;
            }
        }
        #endregion

        // SCREEN DIMENSIONS AS VECTOR2
        /// <summary>
        /// The current screen width as a Vector2. Y component is 0.
        /// </summary>
        public static Vector2 ScreenWidth
        {
            get { return new Vector2 (Screen.width, 0f); }
        }
        /// <summary>
        /// The current screen height as a Vector2. X component is 0.
        /// </summary>
        public static Vector2 ScreenHeight
        {
            get { return new Vector2(0f, Screen.height); }
        }
        /// <summary>
        /// The current screen dimensions as a Vector2.
        /// </summary>
        public static Vector2 ScreenSize
        {
            get { return ScreenWidth + ScreenHeight; }
        }
    }
}