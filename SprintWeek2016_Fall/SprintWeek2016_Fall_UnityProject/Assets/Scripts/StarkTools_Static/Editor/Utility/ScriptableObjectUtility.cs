﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Text;

public static class ScriptableObjectUtility
{
    /// <summary>
    /// Creates a ScriptableObject asset of type T.
    /// </summary>
    /// <typeparam name="T">The type which inherits from ScriptableObject.</typeparam>
    /// <param name="folder">The folder in which to save a new asset.</param>
    /// <remarks>
    /// Add the tag <c>[MenuItem(string)]</c> to the method that calls this to enable
    /// a menu item in the Unity Editor.
    /// </remarks>
    public static void Create<T>(string folder) where T : ScriptableObject
    {
        T scriptableObject = ScriptableObject.CreateInstance<T>();

        AssetDatabase.CreateAsset(
            scriptableObject,
            CreatePath(
                "Assets",
                Path.DirectorySeparatorChar.ToString(),
                folder,
                Path.DirectorySeparatorChar.ToString(),
                typeof(T).ToString(),
                DateTime.UtcNow.Ticks.ToString(),
                ".asset"
                )
            );

        AssetDatabase.SaveAssets();
    }
    /// <summary>
    /// Creates a string from all the provided params.
    /// </summary>
    /// <param name="strings">The strings to concatenate.</param>
    private static string CreatePath(params string[] strings)
    {
        StringBuilder stringBuilder = new StringBuilder();

        foreach (string str in strings)
            stringBuilder.Append(str);

        return stringBuilder.ToString();
    }
}