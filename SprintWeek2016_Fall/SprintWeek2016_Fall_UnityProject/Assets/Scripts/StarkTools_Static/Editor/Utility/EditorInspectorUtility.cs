﻿using UnityEngine;
using System.Collections;

namespace UnityEditor
{
    /// <summary>
    /// A suite of utilities for Editor scripts.
    /// </summary>
    public static class EditorInspectorUtility
    {
        #region GUI Implementation for IEditorSortableList
        private static int buttonWidth = 20;

        /// <summary>
        /// Places a button which adds an item to the list when not entry exists.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="editorTarget"></param>
        /// <param name="list"></param>
        public static void DefaultButton<T>(T editorTarget, IList list) where T : Object, IEditorSortableList
        {
            if (list.Count < 1)
            {
                if (GUILayout.Button("+"))
                {
                    editorTarget.Add(-1);
                }
            }
        }
        /// <summary>
        /// Places [+], [-], [▲], and [▼] buttons.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="editorTarget"></param>
        /// <param name="list"></param>
        /// <param name="i"></param>
        public static void SortableButtons<T>(T editorTarget, IList list, int i) where T : Object, IEditorSortableList
        {
            MoveUpButton(editorTarget, list, i);
            MoveDownButton(editorTarget, list, i);
            AddButton(editorTarget, i);
            RemoveButton(editorTarget, i);
        }
        /// <summary>
        /// Button which adds an elements to the list at the specified <paramref name="index"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="editorTarget"></param>
        /// <param name="i"></param>
        public static void AddButton<T>(T editorTarget, int i) where T : Object, IEditorSortableList
        { 
            if (GUILayout.Button("+", GUILayout.Width(buttonWidth)))
            {
                Undo.RecordObject(editorTarget, "Add new node at index: " + i);
                editorTarget.Add(i + 1);
            }
        }
        /// <summary>
        /// Button which removes an element from the list at the specified <paramref name="index"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="editorTarget"></param>
        /// <param name="i"></param>
        public static void RemoveButton<T>(T editorTarget, int i) where T : Object, IEditorSortableList
        {
            if (GUILayout.Button("-", GUILayout.Width(buttonWidth)))
            {
                Undo.RecordObject(editorTarget, "Remove node at index: " + i);
                editorTarget.Remove(i);
            }
        }
        /// <summary>
        /// Button which moves the element from the specified <paramref name="index"/> down one index.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="editorTarget"></param>
        /// <param name="list"></param>
        /// <param name="i"></param>
        public static void MoveDownButton<T>(T editorTarget, IList list, int i) where T : Object, IEditorSortableList
        {
            if (i < 1) GUI.enabled = false;

            if (GUILayout.Button("▼", GUILayout.Width(buttonWidth)))
            {
                Undo.RecordObject(editorTarget, "Move down.");
                editorTarget.MoveIndex(i, Mathf.Clamp(i + 2, 0, list.Count));
            }

            GUI.enabled = true;
        }
        /// <summary>
        /// Button which moves the element from the specified <paramref name="index"/> up one index.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="editorTarget"></param>
        /// <param name="list"></param>
        /// <param name="i"></param>
        public static void MoveUpButton<T>(T editorTarget, IList list, int i) where T : Object, IEditorSortableList
        {
            if (i > list.Count - 1) GUI.enabled = false;

            if (GUILayout.Button("▲", GUILayout.Width(buttonWidth)))
            {
                Undo.RecordObject(editorTarget, "Move up.");
                editorTarget.MoveIndex(i, Mathf.Clamp(i - 1, 0, list.Count));
            }

            GUI.enabled = true;
        }
        #endregion
    }
}