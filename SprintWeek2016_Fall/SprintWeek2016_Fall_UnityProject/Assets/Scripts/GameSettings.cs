﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using ProjectRed;

public static class GameSettings
{
    private static Dictionary<PlayerTeam, int> TeamScoresRound;
    private static Dictionary<PlayerTeam, int> TeamScoresGames;
    private static PlayerTeam attackingTeam = PlayerTeam.Red;
    private static int currentPoints;
    private static int pointsPerRound = 5;
    private static int pointsPerGame = 2;

    public const float radiusInner = 15f;
    public const float radiusOutter = 12f;
    public const float radiusMidOutter = radiusInner + radiusOutter / 2f;
    public const float radiusTotal = radiusInner + radiusOutter;

    public static Action OnSwitchTeams = delegate { Debug.Log("GameSettings.OnSwitchTeams"); };
    public static Action OnRoundWin = delegate { Debug.Log("GameSettings.OnRoundWin"); };
    public static Action OnGameWin = delegate { Debug.Log("GameSettings.OnGameWin"); };
    public static Action ResetGame = delegate { Debug.Log("GameSettings.ResetGame"); };

    public static Action OnKnockAll = delegate { Debug.Log("GameSettings.OnKnockAll"); };

    public static void TowerToppled()
    {
        TeamScoresRound[attackingTeam]++;
        currentPoints++;
        CheckWinConditions();
    }

    private static void ResetTeamScores(ref Dictionary<PlayerTeam, int> TeamScores)
    {
        TeamScores = new Dictionary<PlayerTeam, int>();
        TeamScores.Add(PlayerTeam.Red, 0);
        TeamScores.Add(PlayerTeam.Blue, 0);
    }
    public static void ResetTeamScoresRound()
    {
        ResetTeamScores(ref TeamScoresRound);
    }
    public static void ResetTeamScoresGames()
    {
        ResetTeamScores(ref TeamScoresGames);
    }
    public static int GetTeamScoreRound(PlayerTeam team)
    {
        return TeamScoresRound[team];
    }
    public static int GetTeamScoreGame(PlayerTeam team)
    {
        return TeamScoresGames[team];
    }

    public static void SwitchTeams()
    {
        if (attackingTeam == PlayerTeam.Red)
            attackingTeam = PlayerTeam.Blue;
        else
            attackingTeam = PlayerTeam.Red;

        OnSwitchTeams.Invoke();
    }
    public static void CheckWinConditions()
    {
        if (currentPoints >= 3)
        {
            currentPoints = 0;
            OnKnockAll.Invoke();
        }

        if (TeamScoresRound[attackingTeam] >= pointsPerRound)
        {
            currentPoints = 0;
            ResetTeamScoresRound();
            TeamScoresGames[attackingTeam]++;

            // GameWin
            if (TeamScoresGames[attackingTeam] >= pointsPerGame)
                OnGameWin.Invoke();
            else
                OnRoundWin.Invoke();
        }

        if (TeamScoresGames[attackingTeam] > pointsPerGame)
        {
            OnGameWin.Invoke();
        }
    }

    public static void InitializeGameSettings()
    {
        ResetGame += delegate { ResetGameSettings(); };
    }
    private static void ResetGameSettings()
    {
        ResetTeamScoresRound();
        ResetTeamScoresGames();
    }






    //
    public static PlayerTeam AttackingTeam
    {
        get
        {
            return attackingTeam;
        }
    }

}
