﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class GenCircle : MonoBehaviour
{
    public float radiusInner = GameSettings.radiusInner;
    public float radiusOutter = GameSettings.radiusOutter;
    public int iterations = 64;
    public float wallDepth = .1f;
    public bool hideMesh;
    public float height = 100f;

    public void Generate()
    {
        transform.ClearTransformImmediate();

        GameObject temp = GameObject.CreatePrimitive(PrimitiveType.Cube);

        for (int i = 0; i < iterations; i++)
        {
            GameObject go = Instantiate(temp);
            go.transform.SetParent(transform);
            go.transform.localPosition = PointOnUnitCircle((float)i / iterations) * (radiusInner + radiusOutter + wallDepth / 2f);
            go.transform.LookAt(transform.position);
            go.transform.localScale = new Vector3((radiusInner + radiusOutter) * Mathf.PI * 2f / iterations, height, wallDepth);

            if (hideMesh)
                go.GetComponent<MeshRenderer>().enabled = false;
        }

        DestroyImmediate(temp);
    }

    public Vector3 PointOnUnitCircle(float percent)
    {
        return new Vector3(Mathf.Cos(percent * Mathf.PI * 2f), 0f, Mathf.Sin(percent * Mathf.PI * 2f));
    }



    void OnDrawGizmos()
    {
        Handles.color = Color.red;
        Handles.DrawWireDisc(Vector3.zero, Vector3.up, radiusInner);

        Handles.color = Palette.green;
        Handles.DrawWireDisc(Vector3.zero, Vector3.up, radiusInner + radiusOutter);
    }
}

[CustomEditor(typeof(GenCircle))]
public class GenCircle_Editor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GenCircle editorTarget = target as GenCircle;

        if (GUILayout.Button("Generate"))
        {
            editorTarget.Generate();
        }
    }
}