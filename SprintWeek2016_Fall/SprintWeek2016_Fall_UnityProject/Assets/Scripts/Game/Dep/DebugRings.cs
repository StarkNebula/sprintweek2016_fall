﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class DebugRings : MonoBehaviour {

    public float radiusInner = 20f;
    public float radiusOutter = 15f;

    void OnDrawGizmos()
    {
        Handles.color = Color.red;
        Handles.DrawWireDisc(Vector3.zero, Vector3.up, radiusInner);

        Handles.color = Palette.green;
        Handles.DrawWireDisc(Vector3.zero, Vector3.up, radiusInner + radiusOutter);
    }
}
