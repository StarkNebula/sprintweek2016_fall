﻿using UnityEngine;
using System.Collections;

namespace ProjectRed
{
    public class Ball : MonoBehaviour, IResetObject
    {
        public AnimationCurve lobArc = AnimationCurveUtility.EaseInOut;
        public Material dodgeball, trail;
        public TrailRenderer trailRenderer;

        public Vector3 velocity;
        public void SetVelocity(Vector3 normal, bool isLob)
        {
            velocity = normal * ((charge / chargeMax) *1.5f + 0.5f) * 75f;
        }
        new public Rigidbody rigidbody;
        public static float gravity = 20f;
        private bool doGravity = true;
        public static float drag = 0.01f;
        public static float wallFriction = 0.2f;

        public float charge { get; private set; }
        public const float chargeMax = 5f;
        public static float chargeFullCooldownTime = 15f;
        public static float hitChargeIncrease = 1f;
        public static float holdChargeIncrease = 1.25f;
        public static float holdChargeDuration = 1f;

        private bool ballHasOwner = false;
        private bool doingLobPass = false;

        int lastIntVal = int.MaxValue;


        void FixedUpdate()
        {
            SetBallColor2();

            // Decrease ball charge over time
            ModifyBallCharge(chargeMax / chargeFullCooldownTime * -Time.deltaTime);

            if (!doingLobPass)
            {
                UpdateVelocityNormal();

                Vector3 pos = transform.position.IgnoreAxis(false, true, false);
                if (pos.magnitude < GameSettings.radiusInner)
                {
                    if (rigidbody.velocity.magnitude < 3f)
                    rigidbody.velocity = rigidbody.velocity.IgnoreAxis(false, true, false).normalized * 3f;
                    Debug.DrawLine(Vector3.zero, pos, Color.black);
                }
            }
        }

        void SetBallColor2()
        {
            if (lastIntVal != (int)charge)
            {
                dodgeball.color = SetBallColor();
                trail.SetColor("_TintColor", dodgeball.color);
                lastIntVal = (int)charge;
            }
        }
        Color SetBallColor()
        {
            switch ((int)charge)
            {
                case 0:
                    return Palette.cobalt;
                case 1:
                    return Palette.green_teal;
                case 2:
                    return Palette.yellow_lime;
                case 3:
                    return Palette.orange;
                case 4:
                case 5:
                    return Palette.red;

                default:
                    return Color.black;
            }
        }

        #region PLAYER PICKUP
        void OnTriggerEnter(Collider collider)
        {
            if (doingLobPass) return;

            if (collider.tag.Equals(Tags.Player))
            {
                if (!ballHasOwner)
                {
                    AvatarController avatar = collider.GetComponent<AvatarController>();

                    if (!avatar.hasBall)
                    {
                        avatar.SetBall(this);
                        velocity = Vector3.zero;
                        HasOwner(true);
                    }
                }
            }
        }
        public void HasOwner(bool value)
        {
            ballHasOwner = value;
        }
        #endregion
        #region GRAVITY AND COLLISION
        void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.tag.Equals(Tags.Untagged) || collision.gameObject.tag.Equals("Ball"))
            {
                velocity = Vector3.Reflect(velocity, collision.ContactNormalsAverage());
                velocity -= velocity * wallFriction;
            }
        }
        void OnCollisionExit()
        {
            doGravity = true;
        }
        void OnCollisionStay()
        {
            doGravity = false;
        }

        void UpdateVelocityNormal()
        {
            // Gravity
            if (doGravity) velocity += Vector3.down * gravity * Time.deltaTime;
            // Drag
            velocity -= velocity * drag;

            // Move ball
            rigidbody.velocity = velocity;
        }

        #endregion
        #region LOB MECHANIC
        public void LobArc(Vector3 start, Vector3 end, Vector3 endVelocity, Transform reticle)
        {
            float duration = 3f - (charge / chargeMax) * 2f;

            if (!doingLobPass)
                StartCoroutine(CoLobArc(duration, start, end, endVelocity, reticle));
        }
        private IEnumerator CoLobArc(float duration, Vector3 start, Vector3 end, Vector3 endVelocity, Transform reticle)
        {
            doingLobPass = true;
            velocity = Vector3.zero;
            reticle.position = end;

            Vector3 directionVector = end - start;
            Vector3 arcVelocity;
            float distance = directionVector.magnitude;
            Vector3 height = Vector3.up * distance * (duration / 3f);

            float timeElapsed = 0;
            while (timeElapsed < duration)
            {
                timeElapsed += Time.deltaTime;

                arcVelocity = directionVector / duration;
                //arcVelocity = Mathf.Cos((timeElapsed / duration) * Mathf.PI - Mathf.PI / 2f) * directionVector * 1.55f;
                arcVelocity += Mathf.Sin((timeElapsed / duration) * Mathf.PI + Mathf.PI / 2f) * height;
                rigidbody.velocity = arcVelocity;

                HasOwner(false); // hack

                yield return new WaitForFixedUpdate();
            }

            // Make ball bounce
            velocity = endVelocity;

            // Reset reticle
            reticle.position = Vector3.up * 1000f;

            doingLobPass = false;
        }
        #endregion

        /// <summary>
        /// Increase or decrease ball charge
        /// </summary>
        /// <param name="value"></param>
        public void ModifyBallCharge(float value)
        {
            charge = Mathf.Clamp(charge + value, 0, chargeMax);
        }


        #region RESET
        private Vector3 position;
        private Quaternion rotation;

        public void ResetObject()
        {
            SetBallColor2();
            HasOwner(false);
            doingLobPass = false;
            transform.parent = null;
            StopAllCoroutines();

            charge = 0f;

            rigidbody.velocity = Vector3.zero;
            rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        }
        #endregion
    }
}