﻿using UnityEngine;
using System.Collections;

public class DefenseWall : MonoBehaviour
{

    public bool useStageForward;

    private bool canPutUpWall = true;
    private float animTime = .15f;
    private float duration = 1f;
    private AnimationCurve curve = AnimationCurveUtility.EaseIn;

    public void PutUpWall(Vector3 position, Quaternion rotation)
    {
        if (canPutUpWall)
        {
            canPutUpWall = false;

            if (useStageForward)
            {
                transform.LookAt(Vector3.up * 1000f + position);
            }
            else
            {
                transform.rotation = rotation;
            }

            StartCoroutine(LerWall(position + Vector3.up * transform.localScale.y / 2f));
        }
    }
    private IEnumerator LerWall(Vector3 end)
    {
        Vector3 start = end + Vector3.down * transform.localScale.y;
        float currentTime = 0f;

        while (currentTime < 1f)
        {
            currentTime += Time.deltaTime / animTime;
            transform.position = Vector3.Lerp(start, end, curve.Evaluate(currentTime));

            yield return new WaitForFixedUpdate();
        }

        transform.position = end;

        yield return new WaitForSeconds(duration);

        StartCoroutine(CloseWall(start));
    }
    private IEnumerator CloseWall(Vector3 end)
    {
        Vector3 start = transform.position;
        float currentTime = 0f;

        while (currentTime < 1f)
        {
            currentTime += Time.deltaTime / animTime / 2f;
            transform.position = Vector3.Lerp(start, end, curve.Evaluate(currentTime));

            yield return new WaitForFixedUpdate();
        }

        transform.position = end;

        canPutUpWall = true;

        transform.position = Vector3.up * 1000f;
    }
}
