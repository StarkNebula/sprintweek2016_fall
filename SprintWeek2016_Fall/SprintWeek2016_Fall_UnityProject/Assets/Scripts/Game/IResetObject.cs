﻿using UnityEngine;
using System.Collections;

public interface IResetObject
{
    void ResetObject();
}
