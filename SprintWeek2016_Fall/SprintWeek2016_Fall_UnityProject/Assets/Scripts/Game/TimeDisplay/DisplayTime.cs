﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DisplayTime : MonoBehaviour {

    public Text timeDisplay;
    
    public void FixedUpdate()
    {
        timeDisplay.text = CountdownTimer.GetTimeAsString();
    } 

}
