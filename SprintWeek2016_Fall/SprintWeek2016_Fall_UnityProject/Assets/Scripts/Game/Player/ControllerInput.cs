﻿using UnityEngine;
using System;

namespace ProjectRed
{
    public static class ControllerInput
    {
        public static float GetInputValue(Xbox360Input input, int playerNumber)
        {
            return Input.GetAxis(string.Format("{0}_P{1}", input.ToString(), playerNumber));
        }
        public static bool GetInputState(Xbox360Input input, int playerNumber)
        {
            return GetInputValue(input, playerNumber) > 0;
        }
        public static bool GetInputStateInverse(Xbox360Input input, int playerNumber)
        {
            return GetInputValue(input, playerNumber) < 0;
        }
        public static bool SetInputState(ref bool wasPressed, Xbox360Input input, int playerNumber)
        {
            bool isPressed = GetInputValue(input, playerNumber) > 0.1f;
            bool returnValue = isPressed && !wasPressed; // Current press true, last press false
            wasPressed = isPressed;

            return returnValue;
        }
        public static Vector3 GetInputAxis(Xbox360Input horizontal, Xbox360Input vertical, int playerNumber)
        {
            return new Vector3(
                GetInputValue(horizontal, playerNumber),
                0,
                GetInputValue(vertical, playerNumber)
                );
        }
    }

    public enum Xbox360Input
    {
        A,
        B,
        X,
        Y,
        LB,
        RB,

        Back,
        Start,

        AnalogueRX,
        AnalogueRY,
        AnalogueLX,
        AnalogueLY,
        DpadX,
        DpadY,
        Triggers,
    }
    public class XboxButton
    {
        public bool isActive;
        public bool wasActive;
        public Action wasPressedAction = delegate { Debug.Log("Pressed: Action not set!"); };
    }
}