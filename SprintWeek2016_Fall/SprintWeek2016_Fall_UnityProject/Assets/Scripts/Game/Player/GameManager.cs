﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using ProjectRed;

public class GameManager : MonoBehaviour
{
    public PlayerInput[] playerControllers;
    public AvatarController[] avatars;
    public Ball[] balls;
    public TowerObject[] towers;
    public Transform[] AttackSpawns, DefenseSpawns, BallSpawns;

    public CountdownTimer countdownTimer;
    public CountdownUI countdownUi;
    public PlayerMenuControls menuControls;

    public void Start()
    {
        // Reset GameSettings
        GameSettings.ResetTeamScoresRound();
        GameSettings.ResetTeamScoresGames();
        menuControls.enabled = false;

        // Round complete
        GameSettings.OnKnockAll += delegate { TimerFinish(); };
        GameSettings.OnRoundWin += delegate { RoundWin(); };
        GameSettings.OnGameWin += delegate { GameWin(); };
        // DELEGATE FOR ACTION
        CountdownTimer.OnTimerComplete += delegate { TimerFinish(); };


        SetArenaTeamPlacement(PlayerTeam.Red);
        CountdownTimer.ResetTimer(CountdownTimer.duration);
        SetActiveScene(false);

        DoCountdown();
    }

    public void DoCountdown()
    {
        countdownUi.DoCountdown(delegate { BeginGame(GameSettings.AttackingTeam); });
    }

    public void StopGame()
    {
        SetActiveScene(false);
        countdownUi.StopAllCoroutines(); // stop countdown
        CountdownTimer.PauseTimer();
    }
    public IEnumerator WaitForInput(string msg, Action action)
    {
        menuControls.enabled = true;

        countdownUi.ui.text = msg;
        while (!PlayerMenuControls.AnyButtonPressed())
        {
            yield return new WaitForFixedUpdate();
        }
        countdownUi.ui.text = "";
        action.Invoke();
        menuControls.enabled = false;
    }

    public void ResumeLoop()
    {
        GameSettings.SwitchTeams();
        SetArenaTeamPlacement(GameSettings.AttackingTeam);
        CountdownTimer.ResetTimer(CountdownTimer.duration);

        DoCountdown();
    }

    public void SetArenaTeamPlacement(PlayerTeam attackingTeam)
    {
        if (attackingTeam == PlayerTeam.Red)
            ResetPlayerBallTower(true);

        else if (attackingTeam == PlayerTeam.Blue)
            ResetPlayerBallTower(false);
    }
    private void ResetPlayerBallTower(bool redIsAttacking)
    {
        Transform[] RedSpawns = PickCorrectSpawns(redIsAttacking);
        Transform[] BlueSpawns = PickCorrectSpawns(!redIsAttacking);

        for (int i = 0; i < 3; i++)
        {
            // Red Team
            playerControllers[i].transform.position = RedSpawns[i].position;
            avatars[i].ResetObject(redIsAttacking);

            // Blue team
            playerControllers[i + 3].transform.position = BlueSpawns[i].position;
            avatars[i + 3].ResetObject(!redIsAttacking);

            // Ball
            balls[i].transform.position = BallSpawns[i].position;
            balls[i].ResetObject();

            // Towers
            towers[i].ResetObject();
        }
    }
    private Transform[] PickCorrectSpawns(bool redIsAttacking)
    {
        return (redIsAttacking) ? AttackSpawns : DefenseSpawns;
    }

    private void BeginGame(PlayerTeam team)
    {
        SetActiveScene(true);
        CountdownTimer.StartTimer();
    }
    public void SetActiveScene(bool value)
    {
        foreach (AvatarController avatar in avatars)
        {
            avatar.enabled = value;
            avatar.rigidbody.velocity = Vector3.zero;
        }

        foreach (Ball ball in balls)
        {
            if (value)
                ball.rigidbody.constraints = RigidbodyConstraints.FreezeAll;
            else
                ball.rigidbody.constraints = RigidbodyConstraints.None;

            ball.rigidbody.velocity = Vector3.zero;
            ball.StopAllCoroutines();
        }

        foreach (TowerObject tower in towers)
        {
            if (value)
                tower.UnFreeze();
            else
                tower.Freeze();
        }
    }


    /// <summary>
    /// When timer finishes, change sides after input
    /// </summary>
    private void TimerFinish()
    {
        StopGame();
        StartCoroutine(WaitForInput("Press A", delegate { ResumeLoop(); }));
    }
    private void RoundWin()
    {
        StopGame();
        StartCoroutine(WaitForInput(GameSettings.AttackingTeam + " won round", delegate { ResumeLoop(); }));
    }
    private void GameWin()
    {
        StopGame();
        StartCoroutine(WaitForInput(GameSettings.AttackingTeam + " won game", delegate { ResumeLoop(); }));
    }

}
