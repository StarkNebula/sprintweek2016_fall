﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using ProjectRed;

public class ScoreDisplay : MonoBehaviour
{
    public Text redRound, blueRound;
    public Text redGame, blueGame;

    public void FixedUpdate()
    {
        redRound.text = GameSettings.GetTeamScoreRound(PlayerTeam.Red).ToString();
        blueRound.text = GameSettings.GetTeamScoreRound(PlayerTeam.Blue).ToString();
        redGame.text = GameSettings.GetTeamScoreGame(PlayerTeam.Red).ToString();
        blueGame.text = GameSettings.GetTeamScoreGame(PlayerTeam.Blue).ToString();
    }
}
