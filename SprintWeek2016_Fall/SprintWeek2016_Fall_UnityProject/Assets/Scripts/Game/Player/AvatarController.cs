﻿using UnityEngine;
using System;
using System.Collections;

namespace ProjectRed
{
    public class AvatarController : MonoBehaviour
    {
        public PlayerInput controller;
        new public Rigidbody rigidbody;

        // JUMP
        public AnimationCurve jumpCurve = AnimationCurveUtility.EaseInOut;
        public bool canJump = true;
        public Vector3 rootPosition;
        public float jumpHeight = 5f;
        public float jumpTime = 1f;

        public float playerSpeed = 3f;
        public float lerpValue = 0.5f;

        public bool hasBall { get; private set; }
        public Ball ball { get; private set; }
        public Transform ballHoldPosition;
        public bool doLob;

        // DEFENSE
        public float radiusIntern = 8f;
        public float degreesCalc = 15f;
        public float degreesSpeed = 90f;

        public bool isAttacking;
        public Transform targetReticle;
        private readonly Vector3 padding = Vector3.up * 0.015f;

        public DefenseWall defenseWall;
        public void PutUpDefenseWall()
        {
            defenseWall.PutUpWall(transform.parent.position + transform.parent.forward * 2f, transform.parent.rotation);
        }
        public Transform radiusMarker;

        public void Awake()
        {
            // record start position
            rootPosition = transform.localPosition;
        }

        public void FixedUpdate()
        {
            if (isAttacking)
                MovementAttack();
            else
                MovementDefense();

            ControllerHook_DisplayTargetPosition();
        }

        public void MovementAttack()
        {
            // Player Movement
            //transform.parent.position += controller.GetAnalogueL * Time.deltaTime * playerSpeed;
            Vector3 velocityOld = rigidbody.velocity;
            Vector3 velocityNew = controller.GetAnalogueL * Time.deltaTime * playerSpeed;

            rigidbody.velocity = Vector3.Lerp(velocityOld, velocityNew, lerpValue);

            if (controller.GetAnalogueL.magnitude > 0.05f)
            {
                Quaternion rotationOld = transform.parent.rotation;
                Quaternion rotationNew = Quaternion.Euler(0, -(Mathf.Atan2(controller.GetAnalogueL.z, controller.GetAnalogueL.x) * Mathf.Rad2Deg - 90f), 0);

                transform.parent.rotation = Quaternion.Lerp(rotationOld, rotationNew, lerpValue * 2f);
            }
        }
        public void MovementDefense()
        {
            float triggers = -ControllerInput.GetInputValue(Xbox360Input.Triggers, controller.playerID);

            radiusIntern = Mathf.Clamp(radiusIntern + triggers / 4f, 5f, GameSettings.radiusInner - 1.5f);

            if (controller.GetAnalogueL.magnitude > 0.05f || Mathf.Abs(triggers) > 0.05f)
            {
                Vector3 direction = controller.GetAnalogueL.normalized;
                if (direction == Vector3.zero)
                    direction = transform.parent.position.normalized;

                Vector3 velocityOld = rigidbody.velocity;
                Vector3 velocityNew = Vector3.RotateTowards(transform.parent.position.normalized, direction, degreesSpeed * Mathf.Deg2Rad, 100f) * radiusIntern;
                velocityNew = (velocityNew - transform.parent.position).normalized * degreesSpeed;

                rigidbody.velocity = Vector3.Lerp(velocityOld, velocityNew, lerpValue);

                SetRadiusMarkerSize();
            }
            else
            {
                rigidbody.velocity = Vector3.Lerp(rigidbody.velocity, Vector3.zero, lerpValue);
            }



            if (controller.GetAnalogueL.magnitude > 0.05f)
            {
                Quaternion rotationOld = transform.parent.rotation;
                Quaternion rotationNew = Quaternion.Euler(0, -(Mathf.Atan2(controller.GetAnalogueL.z, controller.GetAnalogueL.x) * Mathf.Rad2Deg - 90f), 0);

                transform.parent.rotation = Quaternion.Lerp(rotationOld, rotationNew, lerpValue * 2f);
            }
        }
        private void SetRadiusMarkerSize()
        {
            radiusMarker.localScale = new Vector3(radiusIntern, radiusIntern, 0) * 2f;
        }

        public void ResetObject(bool isAttacking)
        {
            this.isAttacking = isAttacking;
            if (isAttacking)
                SetPlayAttackControls();
            else
                SetPlayDefenseControls();

            //
            if (hasBall)
            {
                DetachBallStart(-100f);
                DetachBallEnd();
            }
            StopAllCoroutines();
            transform.localPosition = rootPosition;
            rigidbody.velocity = Vector3.zero;

            canJump = true;

            //
            targetReticle.transform.position = Vector3.up * 1000f;
            transform.parent.LookAt(Vector3.zero);
        }
        #region JUMPING
        public void Jump()
        {
            if (canJump)
                StartCoroutine(JumpLoop(jumpTime));
        }
        private IEnumerator JumpLoop(float duration)
        {
            rigidbody.useGravity = false;

            canJump = false;
            float lerpTime = 0;

            while (lerpTime < 1f)
            {
                lerpTime += Time.deltaTime / duration;
                transform.localPosition = rootPosition + jumpCurve.Evaluate(lerpTime) * Vector3.up * jumpHeight;
                yield return new WaitForFixedUpdate();
            }
            transform.localPosition = rootPosition;
            canJump = true;
            rigidbody.useGravity = true;
        }
        #endregion
        #region BALL LOGIC
        public void SetBall(Ball ball)
        {
            hasBall = true;
            this.ball = ball;
            this.ball.transform.SetParent(ballHoldPosition);
            this.ball.transform.localPosition = Vector3.zero;

            this.ball.rigidbody.constraints = RigidbodyConstraints.FreezeAll;

            if (!isAttacking)
            {
                controller.GetXboxButton[Xbox360Input.Y].wasPressedAction.Invoke();
            }
        }
        public void ChargeAction(float chargeDuration, Xbox360Input inputButton, Action action, float multiplier)
        {
            StartCoroutine(CoChargeBall(inputButton, chargeDuration, action, multiplier));
        }
        private IEnumerator CoChargeBall(Xbox360Input inputButton, float chargeDuration, Action action, float multiplier)
        {
            float timeElapsed = 0f;

            while (ControllerInput.GetInputState(inputButton, controller.playerID) && timeElapsed < chargeDuration)
            {
                if (ball == null) break;

                timeElapsed += Time.deltaTime * multiplier;
                ball.ModifyBallCharge(Ball.holdChargeIncrease * Time.deltaTime);
                yield return new WaitForFixedUpdate();
            }

            action.Invoke();
        }

        public void ShootBall()
        {
            if (ball != null)
            {
                DetachBallStart(Ball.hitChargeIncrease);

                // Lob or not
                if (ControllerInput.GetInputState(Xbox360Input.RB, controller.playerID))
                {
                    Vector3 position = transform.parent.position;
                    ball.LobArc(position, position + transform.parent.forward * position.magnitude + padding, transform.parent.forward * 5f + Vector3.up * 5f, targetReticle);
                }
                else
                    ball.SetVelocity(transform.parent.forward, false);

                DetachBallEnd();
            }
        }
        public void PassBall(float hitCharge)
        {
            if (ball != null)
            {
                DetachBallStart(hitCharge);

                Vector3 startPosition = ball.transform.position;
                Vector3 endPosition = controller.GetAnalogueR.normalized * GameSettings.radiusMidOutter;

                if (endPosition == Vector3.zero)
                    endPosition = -transform.parent.position.normalized * GameSettings.radiusMidOutter;

                ball.LobArc(startPosition, endPosition + padding, Vector3.up * 15f, targetReticle);

                DetachBallEnd();
            }
        }

        public void ControllerHook_DisplayTargetPosition()
        {
            if (!hasBall) return;

            if (ControllerInput.GetInputState(Xbox360Input.RB, controller.playerID))
            {
                if (ControllerInput.GetInputState(Xbox360Input.B, controller.playerID))
                    DisplayTargetPosition(Xbox360Input.B);
                else
                    DisplayTargetPosition(Xbox360Input.RB);
            }
        }

        public void DisplayTargetPosition(Xbox360Input input)
        {
            switch (input)
            {
                case Xbox360Input.RB:
                    if (controller.GetAnalogueR.magnitude > 0.1f)
                    {
                        targetReticle.position = controller.GetAnalogueR.normalized;
                        targetReticle.position *= GameSettings.radiusMidOutter;
                        targetReticle.position += padding;
                    }
                    else
                    {
                        targetReticle.position = -(transform.parent.position.normalized);
                        targetReticle.position *= GameSettings.radiusMidOutter;
                        targetReticle.position += padding;

                    }
                    break;

                case Xbox360Input.B:
                    targetReticle.position = transform.parent.forward * transform.parent.position.magnitude;
                    targetReticle.position += padding;
                    targetReticle.position += transform.parent.position;
                    break;

                default:
                    throw new NotImplementedException();
            }
        }

        public void DetachBallStart(float hitCharge)
        {
            ball.rigidbody.constraints = RigidbodyConstraints.None;
            ball.transform.SetParent(null);
            ball.ModifyBallCharge(hitCharge);
        }
        public void DetachBallEnd()
        {
            ball.HasOwner(false);
            ball = null;
            hasBall = false;
        }
        #endregion
        #region SET BUTTON ACTIONS
        private void SetPlayAttackControls()
        {
            ClearControls();

            targetReticle.gameObject.SetActive(true);
            radiusMarker.gameObject.SetActive(false);

            controller.GetXboxButton[Xbox360Input.A].wasPressedAction = delegate { Jump(); };
            controller.GetXboxButton[Xbox360Input.B].wasPressedAction = delegate { ChargeAction(Ball.holdChargeDuration, Xbox360Input.B, delegate { ShootBall(); }, 1f); };
            controller.GetXboxButton[Xbox360Input.RB].wasPressedAction = delegate { ChargeAction(Ball.holdChargeDuration, Xbox360Input.RB, delegate { PassBall(Ball.hitChargeIncrease); }, 1f); };
        }
        private void SetPlayDefenseControls()
        {
            ClearControls();

            targetReticle.gameObject.SetActive(false);
            radiusMarker.gameObject.SetActive(true);
            SetRadiusMarkerSize();

            controller.GetXboxButton[Xbox360Input.A].wasPressedAction = delegate { Jump(); };
            controller.GetXboxButton[Xbox360Input.B].wasPressedAction = delegate { PutUpDefenseWall(); };
            controller.GetXboxButton[Xbox360Input.Y].wasPressedAction = delegate { PassBall(Ball.holdChargeDuration * -2f); };
            controller.GetXboxButton[Xbox360Input.RB].wasPressedAction = delegate { ChargeAction(Ball.holdChargeDuration, Xbox360Input.RB, delegate { PassBall(-Ball.hitChargeIncrease * 2f); }, -1f); };
        }

        public void ClearControls()
        {
            controller.ResetAllButtonActions(delegate { });
        }
        #endregion

    }
}