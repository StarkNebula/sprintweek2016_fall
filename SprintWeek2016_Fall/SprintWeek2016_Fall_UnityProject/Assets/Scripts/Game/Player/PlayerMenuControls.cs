﻿using UnityEngine;
using System.Collections;
using ProjectRed;

public class PlayerMenuControls : MonoBehaviour
{
    public static bool[] aButtons = new bool[6];

    void FixedUpdate()
    {
        for (int i = 0; i < 6; i++)
            if (ControllerInput.GetInputState(Xbox360Input.A, i+1))
                aButtons[i] = true;
    }

    public static bool AnyTeamPressed(PlayerTeam team)
    {
        int offset = (team == PlayerTeam.Red) ? 0 : 3;

        for (int i = 0 + offset; i < 3 + offset; i++)
            if (aButtons[i])
            {
                aButtons = new bool[6];
                return true;
            }

        return false;
    }
    public static bool AnyButtonPressed()
    {
        for (int i = 0; i < 6; i++)
        {
            if (aButtons[i])
            {
                aButtons = new bool[6];
                return true;
            }
        }

        return false;
    }
}
