﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace ProjectRed
{
    public class PlayerInput : MonoBehaviour
    {
        #region variables
        public int playerID = 1;
        public AvatarController avatar;
        private XboxButton[] xboxButtons;
        public Dictionary<Xbox360Input, XboxButton> GetXboxButton { get; private set; }

        Color playerColor = Color.red;
        #endregion

        public PlayerInput(int id, AvatarController avatar)
        {
            playerID = id;
            this.avatar = avatar;
        }

        #region INPUT INITIALIZATION AND UPDATE
        void Awake()
        {
            InitializeButtonStates();
        }
        public void Update()
        {
            UpdateButtonStates();
            //DoButtonActions();
        }
        #endregion

        #region BUTTON LOGIC
        public void InitializeButtonStates()
        {
            xboxButtons = new XboxButton[8];
            for (int i = 0; i < xboxButtons.Length; i++)
                xboxButtons[i] = new XboxButton();

            GetXboxButton = new Dictionary<Xbox360Input, XboxButton>();
            GetXboxButton.Add(Xbox360Input.A, xboxButtons[0]);
            GetXboxButton.Add(Xbox360Input.B, xboxButtons[1]);
            GetXboxButton.Add(Xbox360Input.X, xboxButtons[2]);
            GetXboxButton.Add(Xbox360Input.Y, xboxButtons[3]);
            GetXboxButton.Add(Xbox360Input.LB, xboxButtons[4]);
            GetXboxButton.Add(Xbox360Input.RB, xboxButtons[5]);
            GetXboxButton.Add(Xbox360Input.Back, xboxButtons[6]);
            GetXboxButton.Add(Xbox360Input.Start, xboxButtons[7]);
        }
        public void UpdateButtonStates()
        {
            SetInputStateInternal(Xbox360Input.A, playerID);
            SetInputStateInternal(Xbox360Input.B, playerID);
            //SetInputStateInternal(Xbox360Input.X, playerID);
            SetInputStateInternal(Xbox360Input.Y, playerID);
            //SetInputStateInternal(Xbox360Input.LB, playerID);
            SetInputStateInternal(Xbox360Input.RB, playerID);
            //SetInputStateInternal(Xbox360Input.Back, playerID);
            //SetInputStateInternal(Xbox360Input.Start, playerID);
        }
        public void SetInputStateInternal(Xbox360Input input, int playerID)
        {
            bool wasActive = GetXboxButton[input].wasActive;

            GetXboxButton[input].isActive = ControllerInput.SetInputState(ref wasActive, input, playerID);
            GetXboxButton[input].wasActive = wasActive;

            if (GetXboxButton[input].isActive)
                GetXboxButton[input].wasPressedAction.Invoke();
        }

        public void ResetAllButtonActions(Action action)
        {
            foreach (XboxButton button in xboxButtons)
                button.wasPressedAction = action;
        }
        #endregion

        #region ANALOGUE INPUT
        public Vector3 GetAnalogueL
        {
            get
            {
                Vector3 input = ControllerInput.GetInputAxis(Xbox360Input.AnalogueLX, Xbox360Input.AnalogueLY, playerID);
                input = new Vector3(input.x, 0, -input.z);
                return input;
            }
        }
        public Vector3 GetAnalogueR
        {
            get
            {
                Vector3 input = ControllerInput.GetInputAxis(Xbox360Input.AnalogueRX, Xbox360Input.AnalogueRY, playerID);
                input = new Vector3(input.x, 0, -input.z);
                return input;
            }
        }
        public Vector3 GetDpad
        {
            get
            {
                Vector3 input = ControllerInput.GetInputAxis(Xbox360Input.DpadX, Xbox360Input.DpadY, playerID);
                input = new Vector3(input.x, 0, input.z);
                return input;
            }
        }
        public bool DpadUp
        {
            get
            {
                return ControllerInput.GetInputState(Xbox360Input.DpadY, playerID);
            }
        }
        public bool DpadDown
        {
            get
            {
                return ControllerInput.GetInputStateInverse(Xbox360Input.DpadY, playerID);
            }
        }
        public bool DpadLeft
        {
            get
            {
                return ControllerInput.GetInputStateInverse(Xbox360Input.DpadX, playerID);
            }
        }
        public bool DpadRight
        {
            get
            {
                return ControllerInput.GetInputState(Xbox360Input.DpadX, playerID);
            }
        }
        #endregion


    }

    // Reference all players via this list
    //public static List<PlayerInput> players = new List<PlayerInput>();
    public enum PlayerTeam
    {
        None,
        Red,
        Blue,
    }

}