﻿using UnityEngine;
using System.Collections;

public class TowerObject : MonoBehaviour, IResetObject
{
    new public Rigidbody rigidbody;
    new public MeshRenderer renderer;

    private float timestamp;
    public Transform heightMarker;
    public Material towerStable;
    public Material towerToppled;
    private bool isKnockedOver;

    public Vector3 position;
    private Quaternion rotation;

    void Awake()
    {
        position = transform.localPosition;
        rotation = transform.localRotation;

        rigidbody.centerOfMass += Vector3.up * transform.localScale.y / 4f;
    }

    void FixedUpdate()
    {
        if (!isKnockedOver)
        if (HasBeenTimeSinceLastTrue(.5f))
            if (heightMarker.transform.position.y < 1f)
                OnKnockedOver();
    }


    bool HasBeenTimeSinceLastTrue(float time)
    {
        bool b = (Time.time - timestamp > time);
        if (b) timestamp = Time.time;
        return b;
    }

    void OnKnockedOver()
    {
        gameObject.layer = LayerMask.NameToLayer("NonInteractive");
        renderer.material = towerToppled;
        GameSettings.TowerToppled();
        isKnockedOver = true;
    }
    public void Freeze()
    {
        rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        rigidbody.velocity = Vector3.zero;
    }
    public void UnFreeze()
    {
        rigidbody.constraints = RigidbodyConstraints.None;
        rigidbody.velocity = Vector3.zero;
    }

    public void ResetObject()
    {
        transform.localPosition = position;
        transform.localRotation = rotation;

        gameObject.layer = LayerMask.NameToLayer("Default");
        renderer.material = towerStable;
        isKnockedOver = false;
        rigidbody.velocity = Vector3.zero;
        rigidbody.constraints = RigidbodyConstraints.None;
    }

    void OnDrawGizmos()
    {
        UnityEditor.Handles.color = Color.red;
        UnityEditor.Handles.DrawWireDisc(transform.position + rigidbody.centerOfMass, transform.up, 1f);
        UnityEditor.Handles.DrawWireDisc(heightMarker.position, heightMarker.up, .5f);
    }
}
