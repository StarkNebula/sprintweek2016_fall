﻿using UnityEngine;
using System;

public class CountdownTimer : MonoBehaviour
{
    public static float timer;
    public static bool timerActive;
    public static float duration = 45;
    public static Action OnTimerComplete = delegate { Debug.Log("OnTimerComplete"); };
    public static Action OnTimerActivate = delegate { Debug.Log("OnTimerActivate"); };
    public static Action OnTimerDeactivate = delegate { Debug.Log("OnTimerDeactivate"); };

    void FixedUpdate()
    {
        if (timerActive)
        {
            timer -= Time.deltaTime;

            if (timer <= 0)
            {
                timer = 0;
                PauseTimer();
                OnTimerComplete.Invoke();
            }
        }
    }

    public static void StartTimer()
    {
        timer = duration;
        ResumeTimer();
        ResetTimer(duration);
    }
    public static void StopTimer()
    {
        PauseTimer();
        ResetTimer(duration);
    }
    public static void PauseTimer()
    {
        timerActive = false;
        OnTimerDeactivate.Invoke();
    }
    public static void ResumeTimer()
    {
        timerActive = true;
        OnTimerActivate.Invoke();
    }
    public static void ResetTimer(float _duration)
    {
        timer = _duration;
        duration = _duration;
    }

    public static string GetTimeAsString()
    {
        return TimeUtility.TimeDisplay(timer, true, true, false, false, ':');
        //return TimeUtility.SecondsOnly(timer);
    }
}
