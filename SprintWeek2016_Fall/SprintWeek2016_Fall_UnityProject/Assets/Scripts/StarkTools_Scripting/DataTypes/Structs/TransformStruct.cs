﻿namespace UnityEngine
{
    [System.Serializable]
    public struct TransformStruct
    {
        public Vector3 position;
        public Quaternion rotation;
        public Vector3 scale;

        public TransformStruct(Vector3 pos, Quaternion rot, Vector3 sca)
        {
            position = pos;
            rotation = rot;
            scale = sca;
        }
    }
}