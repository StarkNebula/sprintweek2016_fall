﻿[System.Serializable]
public struct Bool3
{
    // COMPONENTS
    public bool x;
    public bool y;
    public bool z;

    // CONSTRUCTORS
    public Bool3(bool x, bool y, bool z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    // OPERATORS
    /// <summary>
    /// If either bool component is true, return true
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    //[System.Obsolete("Use | (OR operator) instead!")]
    public static Bool3 operator +(Bool3 a, Bool3 b)
    {
        return new Bool3(
            a.x | b.x,
            a.y | b.y,
            a.z | b.z);
    }
    /// <summary>
    /// If operand B is true, return false
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    public static Bool3 operator -(Bool3 a, Bool3 b)
    {
        return new Bool3(
            b.x ? false : true,
            b.y ? false : true,
            b.z ? false : true);
    }
    /// <summary>
    /// Uses OR operator on all components
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    public static Bool3 operator |(Bool3 a, Bool3 b)
    {
        return new Bool3(
            a.x | b.x,
            a.y | b.y,
            a.z | b.z);
    }
    /// <summary>
    /// Uses AND operator on all components
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    public static Bool3 operator &(Bool3 a, Bool3 b)
    {
        return new Bool3(
            a.x & b.x,
            a.y & b.y,
            a.z & b.z);
    }
    /// <summary>
    /// Uses XOR operator on all components
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    public static Bool3 operator ^(Bool3 a, Bool3 b)
    {
        return new Bool3(
            a.x ^ b.x,
            a.y ^ b.y,
            a.z ^ b.z);
    }
}
