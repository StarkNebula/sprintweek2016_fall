﻿/* GNU License Agreement
    Extends Unity Editor adding Snap To Floor functionality found in Unreal Editor
    Copyright(C) 11/04/2016 Raphael "StarkNebula" Tetreault 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see<http://www.gnu.org/licenses/>.
*/
/* MENUITEM REMINDER
    % CTRL
    & ALT
    # SHIFT
    _ no modifiers
    LEFT, RIGHT, UP, DOWN
    F1 .. F12
    HOME, END, PGUP, PGDN
*/

using UnityEngine;

namespace UnityEditor
{
    public static class SnapToFloor
    {
        public static void SweepToFloor(Vector3 direction)
        {
            direction = direction.normalized;
            Undo.RecordObjects(Selection.transforms, "SweepToFloor");

            foreach (Transform transform in Selection.transforms)
            {
                if (transform.GetComponent<Collider>() != null)
                {
                    bool needsRigidbody = false;
                    RaycastHit raycastInfo;

                    if (transform.GetComponent<Rigidbody>() == null)
                    {
                        needsRigidbody = true;
                        transform.gameObject.AddComponent<Rigidbody>();
                    }

                    // If sweep test hits, it returns true
                    if (transform.GetComponent<Rigidbody>().SweepTest(direction, out raycastInfo))
                        transform.position += direction * raycastInfo.distance;

                    if (needsRigidbody)
                        Object.DestroyImmediate(transform.GetComponent<Rigidbody>());
                }
            }
        }


        [MenuItem("EditorTools/SweepToFloor %DOWN")]
        public static void SweepToFloorEnd()
        {
            SweepToFloor(Vector3.down);
        }
        [MenuItem("EditorTools/SweepToCeiling %UP")]
        public static void SweepToCeilingEnd()
        {
            SweepToFloor(Vector3.up);
        }


        [MenuItem("EditorTools/RayToFloor %&DOWN")]
        public static void RayToFloor()
        {
            Undo.RecordObjects(Selection.transforms, "RayToFloor");

            foreach (Transform transform in Selection.transforms)
            {
                RaycastHit raycastInfo;
                if (Physics.Raycast(transform.position, Vector3.down, out raycastInfo))
                    transform.position += Vector3.down * raycastInfo.distance;
            }
        }
        // Overload with Shift End
        //[MenuItem("EditorTools/RayToFloor &END")]
        public static void RayToFloorEnd()
        {
            RayToFloor();
        }
    }
}