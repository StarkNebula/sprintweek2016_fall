﻿// Created by Raphael "Stark" Tetreault 16/09/2016
// Copyright © 2016 Raphael Tetreault
// Last updated 17/09/2016

using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;

namespace UnityEditor
{
    public class CopyRepository
    {
        #region Preferences
        private static bool prefsLoaded = false;
        public static string m_inputPath, m_outputPath;
        public static List<string> preserveFolders = new List<string>();
        public static int foldersCount;

        private const int width = 80;
        #endregion

        private static string DefaultPath
        {
            get
            {
                return Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.Personal)).FullName;
            }
        }

        [MenuItem("EditorTools/Copy Assets Folder to Extern")]
        public static void CopyAssetsToExtern()
        {
            LoadPreferences();
            SaveToExternPrompt(m_inputPath, m_outputPath);
        }
        private static void SaveToExternPrompt(string inputPath, string outputPath)
        {
            bool accept = EditorUtility.DisplayDialog(
                "Overwrite Folder",
                "This operation will irreversibly overwrite the contents of:\n\n" + outputPath + "\n\nContinue?",
                "Overwrite",
                "Cancel"
                );

            if (accept)
                SaveToExternPromptConfirm(inputPath, outputPath);
            else
                Debug.LogWarning("Save to Extern aborted!");
        }
        private static void SaveToExternPromptConfirm(string inputPath, string outputPath)
        {
            bool decline = EditorUtility.DisplayDialog(
                "Overwrite Folder",
                "Are you certain you want to irreversibly overwrite the contents of:\n\n" + outputPath + "\n\nTHIS IS PERMANENT!",
                "Cancel",
                "OVERWRITE"
                );

            if (!decline)
                SaveToExternOverwriteSafe(inputPath, outputPath);
            else
                Debug.LogWarning("Save to Extern aborted!");
        }
        private static void SaveToExternOverwrite(string inputPath, string outputPath)
        {
            // Replace directory with current Assets Directory, to the 
            // exception of the listed preserved folders.
            PreserveFolders(
                outputPath,
                delegate () { FileUtil.ReplaceDirectory(inputPath, outputPath); },
                preserveFolders.ToArray()
                );

            EditorUtility.DisplayDialog(
                "Repository Transfer",
                "Save to extern successful! Output to:\n\n" + outputPath,
                "OK"
                );

        }
        private static void SaveToExternOverwriteSafe(string inputPath, string outputPath)
        {
            try
            {
                SaveToExternOverwrite(inputPath, outputPath);
            }
            catch (IOException io)
            {
                Debug.LogError(io.Message + "\n" + io.StackTrace);
                Directory.CreateDirectory(m_outputPath); // Create output path which is missing

                // Inform user
                EditorUtility.DisplayDialog(
                    "Invalid Folder Path",
                    "Output path \n\n" + m_outputPath + "\n\ndid not exist. Creating path...",
                    "OK"
                    );

                // Perform save again
                SaveToExternOverwrite(inputPath, outputPath);
            }
        }

        // Saving, Loading
        private static void LoadPreferences()
        {
            if (!prefsLoaded)
            {
                m_inputPath = EditorPrefs.GetString("inputPath", Application.dataPath);
                m_outputPath = EditorPrefs.GetString("outputPath", DefaultPath);
                foldersCount = EditorPrefs.GetInt("foldersCount", preserveFolders.Count);

                for (int i = 0; i < foldersCount; i++)
                    preserveFolders.Add(EditorPrefs.GetString("preserveFolders" + i.ToString(), null));

                prefsLoaded = true;
            }
        }
        private static void SavePreferences()
        {
            EditorPrefs.SetString("inputPath", m_inputPath);
            EditorPrefs.SetString("outputPath", m_outputPath);
            EditorPrefs.SetInt("foldersCount", preserveFolders.Count);
            for (int i = 0; i < preserveFolders.Count; i++)
                EditorPrefs.SetString("preserveFolders" + i.ToString(), preserveFolders[i]);
        }

        // GUI
        [PreferenceItem("Copy to Extern")]
        public static void PreferencesGUI()
        {
            LoadPreferences();

            #region Preferences GUI
            // Output Path
            m_inputPath = BrowseTextField("Input Path", "Input Path", m_inputPath, m_inputPath, GUILayout.Width(width));
            m_outputPath = BrowseTextField("Output Path", "Output Path", m_outputPath, m_outputPath, GUILayout.Width(width));

            EditorGUILayout.Separator();

            if (GUILayout.Button("Add a folder to preserve. [+]"))
                preserveFolders.Add(null);

            for (int i = 0; i < preserveFolders.Count; i++)
            {
                GUILayout.BeginHorizontal();
                preserveFolders[i] = BrowseTextField(i.ToString(), "Preserve Folder", m_outputPath, preserveFolders[i], GUILayout.MinWidth(0));
                if (GUILayout.Button("-"))
                    preserveFolders.RemoveAt(i);
                GUILayout.EndHorizontal();
            }

            EditorGUILayout.Separator();

            if (GUILayout.Button("Save to Extern"))
                CopyAssetsToExtern();
            #endregion

            if (GUI.changed)
                SavePreferences();
        }
        private static string BrowseTextField(string label, string title, string browsePath, string value, GUILayoutOption labelLayout)
        {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label(label, labelLayout);
            value = EditorGUILayout.TextField(value);

            if (GUILayout.Button("Browse"))
            {
                string str = EditorUtility.OpenFolderPanel(title, browsePath, value);

                // Min requirements for C:/, etc.
                // Prevents returning null on cancel.
                if (str.Length > 2)
                    value = str;
            }
            EditorGUILayout.EndHorizontal();

            return value;
        }

        public static void PreserveFolders(string outputDirectory, Action method, params string[] folders)
        {
            // The folder which will house all other folders temporarily
            string tempDirectory = Path.Combine(DefaultPath, "CopyRepository" + DateTime.Now.Ticks);
            Directory.CreateDirectory(tempDirectory);

            string[] src = new string[folders.Length];
            string[] dst = new string[folders.Length];
            DirectoryInfo[] di = new DirectoryInfo[folders.Length];

            // Moves folders from folder that will be overwritten to temp location
            for (int i = 0; i < folders.Length; i++)
            {
                if (Directory.Exists(folders[i]))
                {
                    // Prevents Windows from having a directory with / and \ separators
                    src[i] = folders[i].Replace("/", Path.DirectorySeparatorChar.ToString());
                    dst[i] = Path.Combine(tempDirectory, i.ToString());
                    di[i] = new DirectoryInfo(src[i]);

                    //Debug.Log(src[i]);
                    //Debug.Log(dst[i]);

                    // For whatever reason, calling di[i].Attributes prevents DirectoryInformation from
                    // purging the directory attributes. IDFK.
                    Debug.Log("Saving Attributes: " + src[i] + " :: " + di[i].Attributes);

                    if (Directory.Exists(src[i]))
                        FileUtil.MoveFileOrDirectory(src[i], dst[i]);
                }
            }

            // The method that gets invoke.
            // ie: FileUtil.ReplaceDirectory()
            method.Invoke();

            // Moves folders back to destination folder
            for (int i = 0; i < folders.Length; i++)
            {
                if (Directory.Exists(dst[i]))
                {
                    FileUtil.MoveFileOrDirectory(dst[i], src[i]);
                    DirectoryInfo fdi = new DirectoryInfo(src[i]);
                    fdi.Attributes = di[i].Attributes;
                }
            }

            // Remove temp folder
            Directory.Delete(tempDirectory);
        }
    }
}