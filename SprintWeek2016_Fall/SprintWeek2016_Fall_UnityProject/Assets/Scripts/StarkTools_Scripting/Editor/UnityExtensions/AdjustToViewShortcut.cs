﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class AdjustToViewShortcut
{
    [MenuItem("EditorTools/Align Camera and Objects to view _HOME")]
    public static void AlignToView()
    {
        List<Object> transforms = new List<Object>(Selection.transforms);
        transforms.Add(Camera.main.transform);

        Undo.RecordObjects(transforms.ToArray(), "Align camera and selection to view.");

        foreach (Transform t in transforms)
        {
            t.position = Camera.current.transform.position;
            t.rotation = Camera.current.transform.rotation;
        }
    }
}
