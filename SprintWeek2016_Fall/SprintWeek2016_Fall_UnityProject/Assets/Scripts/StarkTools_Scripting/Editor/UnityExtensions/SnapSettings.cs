﻿using UnityEngine;

namespace UnityEditor
{
    public static class SnapSettings
    {
        private static float snapValue = 1f;
        private static float Offset { get { return snapValue / 2f; } }

        [MenuItem("EditorTools/Snap Odd %")]
        public static void Snap()
        {
            Undo.RecordObjects(Selection.transforms, "Snap to grid");

            foreach (Transform transform in Selection.transforms)
            {
                transform.position = new Vector3(
                    MathX.RoundValue(transform.position.x, snapValue) + Offset,
                    MathX.RoundValue(transform.position.y, snapValue) + Offset,
                    MathX.RoundValue(transform.position.z, snapValue) + Offset);
            }
        }
    }
}
