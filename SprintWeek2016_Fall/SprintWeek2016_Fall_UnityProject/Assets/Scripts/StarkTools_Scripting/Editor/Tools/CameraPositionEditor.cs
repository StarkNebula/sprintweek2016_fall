﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(CameraPosition))]
public class CameraPositionEditor : Editor {

    public bool foldout;

    public override void OnInspectorGUI()
    {
        //DrawDefaultInspector();
        CameraPosition editorTarget = target as CameraPosition;

        editorTarget.DebugPositions = EditorGUILayout.ObjectField("Container", editorTarget.DebugPositions, typeof(Transform), true) as Transform;

        editorTarget.camera = EditorGUILayout.ObjectField("Camera", editorTarget.camera, typeof(Camera), true) as Camera;
        editorTarget.lerpTime = EditorGUILayout.FloatField("Lerp Time", editorTarget.lerpTime);
        editorTarget.animationCurve = EditorGUILayout.CurveField("Animation Curve", editorTarget.animationCurve);

        // Alpha
        GUILayout.BeginHorizontal();
        GUILayout.Label("Alpha");
        editorTarget.guiAlpha = GUILayout.HorizontalSlider(editorTarget.guiAlpha, 0f, 1f);
        GUILayout.Label((editorTarget.guiAlpha * 100f).ToString("0") + "%", GUILayout.Width(40));
        GUILayout.EndHorizontal();
        GUILayout.Space(10);

        // Record
        if (GUILayout.Button("Record Position"))
            editorTarget.RecordPosition();
        GUILayout.Space(10);

        // Display
        for (int i = 0; i < editorTarget.trans.Count; i++)
        {
            GUILayout.BeginHorizontal();

            // Draw warp To button
            GUI.color = Palette.Mix(Palette.ColorWheel(i, 24), Palette.white);
            if (GUILayout.Button("Got to [Pos " + editorTarget.trans[i].position + " ]"))// + " [Rot " + editorTarget.trans[i].rotation.eulerAngles + " ]"))
                editorTarget.WarpToPosition(i);

            // Draw delete button
            GUI.color = Palette.Mix(Palette.red, Palette.white);
            if (GUILayout.Button("Delete", GUILayout.Width(120f)))
                editorTarget.DeletePosition(i);

            GUILayout.EndHorizontal();

            foldout = EditorGUILayout.Foldout(foldout, "Full Object Parameters");
            if (foldout)
            {
                GUI.color = Palette.white;
                editorTarget.trans[i] = EditorGUILayout.ObjectField("Object", editorTarget.trans[i], typeof(Transform), true) as Transform;
                editorTarget.trans[i].position = EditorGUILayout.Vector3Field("Position", editorTarget.trans[i].position);
                editorTarget.trans[i].rotation = Quaternion.Euler( EditorGUILayout.Vector3Field("Rotation", editorTarget.trans[i].rotation.eulerAngles));
                editorTarget.trans[i].localScale = EditorGUILayout.Vector3Field("Scale", editorTarget.trans[i].localScale);

                GUILayout.Space(10f);

                editorTarget.Fov[i] = EditorGUILayout.Slider("FOV", editorTarget.Fov[i], 1f, 179f);

                editorTarget.viewport[i] = EditorGUILayout.RectField("Camera Viewport" ,editorTarget.viewport[i]);
            }
        }
        
        GUI.color = Palette.white;
        if (GUILayout.Button("TEST"))
            editorTarget.Test();
    }

}
