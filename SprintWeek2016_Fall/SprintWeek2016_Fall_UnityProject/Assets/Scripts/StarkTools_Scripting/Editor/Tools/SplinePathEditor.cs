﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(SplinePath))]
public class SplinePathEditor : Editor {

    private int indent = 50;

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        SplinePath editorTarget = target as SplinePath;

        GUILayout.BeginHorizontal();
        editorTarget.isLooping = EditorGUILayout.ToggleLeft("Path Loops", editorTarget.isLooping);
        editorTarget.isDotted = EditorGUILayout.ToggleLeft("Dotted Path", editorTarget.isDotted);
        GUILayout.EndHorizontal();



        GUILayout.Space(10);

        editorTarget.entireCycleTime = EditorGUILayout.FloatField("Cycle Time", editorTarget.entireCycleTime);
        GUILayout.Label("Travel Speed: " + (editorTarget.pathLength / editorTarget.entireCycleTime).ToString("0.0") + " units per second" );

        GUILayout.Space(10);


        editorTarget.steps = EditorGUILayout.FloatField("Steps per Segment", editorTarget.steps);

        //
        //for (int i = 0; i < editorTarget.lengthPerSegment.Length; i++)
        //EditorGUILayout.SelectableLabel(editorTarget.lengthPerSegment[i].ToString());


        if (editorTarget.rb.Count == 0)
            if (GUILayout.Button("Insert New Entry"))
                editorTarget.AddField(0);

        for (int i = 0; i < editorTarget.rb.Count; i++)
        {
            GUILayout.Space(15);

            GUI.color = Palette.Mix(
                Palette.ColorWheel(i, 12),
                Palette.white);

            GUILayout.BeginHorizontal();
            GUILayout.Label((i+1).ToString());
            editorTarget.rb[i] = EditorGUILayout.ObjectField(editorTarget.rb[i], typeof(Rigidbody), true) as Rigidbody;
            GUILayout.EndHorizontal();

            GUI.color = Palette.white;

            GUILayout.BeginHorizontal();
            GUILayout.Space(indent);
            GUILayout.Label("Segment ", GUILayout.Width(80));
            editorTarget.currentSegment[i] = EditorGUILayout.IntSlider(editorTarget.currentSegment[i], 0, editorTarget.keyPosition.Length - 1);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Space(indent);
            GUILayout.Label("Step ", GUILayout.Width(80));
            editorTarget.currentStep[i] = EditorGUILayout.Slider(editorTarget.currentStep[i], 0, editorTarget.steps);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Space(indent);
            //GUI.color = Palette.Mix(Palette.gre);
            if (GUILayout.Button("Insert New Entry"))
                editorTarget.AddField(i+1);

            //GUI.color = Palette.Mix(Palette.red, 5f, Palette.white);
            if (GUILayout.Button("Delete"))
                editorTarget.DeleteEntry(i);
            GUILayout.EndHorizontal();

            GUI.color = Palette.white;
        }

        if (GUI.changed)
        {
            EditorUtility.SetDirty(target);
        }

    }

}
