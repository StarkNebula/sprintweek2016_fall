﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(WarpTo))]
public class WarpToEditor : Editor
{

    public override void OnInspectorGUI()
    {
        //DrawDefaultInspector();
        WarpTo warpTo = (WarpTo)target;

        // Alpha
        GUILayout.BeginHorizontal();
        //GUILayout.Label("Alpha");
        warpTo.guiAlpha = GUILayout.HorizontalSlider(warpTo.guiAlpha, 0f, 1f);
        GUILayout.Label((warpTo.guiAlpha * 100f).ToString("0") + "%", GUILayout.Width(40));
        GUILayout.EndHorizontal();

        GUILayout.Space(10);

        // Record
        if (GUILayout.Button("Record Position"))
            warpTo.RecordPosition();

        GUILayout.Space(10);

        // Display buttons
        for (int i = 0; i < warpTo.transforms.Count; i++)
        {
            GUILayout.BeginHorizontal();
            
            // Warp To button
            GUI.color = Palette.ColorWheel(i, 6).Whitten(.5f);
            if (GUILayout.Button("Warp To: [Pos " + warpTo.transforms[i].position + " ] [Rot " + warpTo.transforms[i].rotation.eulerAngles + " ]"))
            {
                Undo.RecordObject(warpTo, "Record position.");
                warpTo.WarpToPosition(i);
            }

            // Up/Down button
            GUI.color = Palette.white;
            if (GUILayout.Button("\u25b2".ToString(), GUILayout.Width(20f)))
            {
                Undo.RecordObject(warpTo, "Move down.");
                warpTo.MoveIndex(i, Mathf.Clamp(i + 2, 0, warpTo.transforms.Count));
            }
            if (GUILayout.Button("\u25bc", GUILayout.Width(20f)))
            {
                Undo.RecordObject(warpTo, "Move up.");
                warpTo.MoveIndex(i, Mathf.Clamp(i - 1, 0, warpTo.transforms.Count));
            }

            // Insert Above / Delete
            if (GUILayout.Button("+", GUILayout.Width(20f)))
            {
                Undo.RecordObject(warpTo, "Insert position.");
                warpTo.RecordPosition(i);
            }
            GUI.color = Palette.Mix(Palette.red, Palette.white);
            if (GUILayout.Button("-", GUILayout.Width(20f)))
            {
                Undo.RecordObject(warpTo, "Delete position.");
                warpTo.DeletePosition(i);
            }
            GUILayout.EndHorizontal();

        }
    }

}
