﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(StarPatternGenerator))]
public class StarPatternGeneratorEditor : Editor
{
    // MAIN
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        StarPatternGenerator editorTarget = target as StarPatternGenerator;

        // PATTERN
        editorTarget.pattern = (Pattern)EditorGUILayout.EnumPopup("Selected Pattern", editorTarget.pattern);
        editorTarget.secondsPerCycle = EditorGUILayout.FloatField("Seconds Per Cycle", editorTarget.secondsPerCycle);
        editorTarget.offset = EditorGUILayout.Vector2Field("Offset", editorTarget.offset);

        EditorGUILayout.Separator();

        // TABS
        DebugInformationTab(ref editorTarget);
        if (Foldout(editorTarget, Display.DebugInformation)) EditorGUILayout.Separator();

        RotationTab(ref editorTarget);
        if (Foldout(editorTarget, Display.Rotation)) EditorGUILayout.Separator();

        ScaleTab(ref editorTarget);
        if (Foldout(editorTarget, Display.Scale)) EditorGUILayout.Separator();

        // Parameters
        editorTarget.foldout[(int)Display.Parameters] = EditorGUILayout.Foldout(editorTarget.foldout[(int)Display.Parameters], (editorTarget.pattern.ToString() + " Parameters").ToUpper());
        if (editorTarget.foldout[(int)Display.Parameters])
        {
            if (editorTarget.pattern == Pattern.FarrisWheel)
                FarrisWheelTab(ref editorTarget);
            else
                GeneralPatternTab(ref editorTarget);
        }
        if (Foldout(editorTarget, Display.Parameters)) EditorGUILayout.Separator();

        SatellitesTab(ref editorTarget);
        if (Foldout(editorTarget, Display.Satellites)) EditorGUILayout.Separator();

        // UPDATE
        // Update all variable changes in the scene view
        editorTarget.UpdateSatellitePositions();
        SceneView.RepaintAll();
    }

    // DISPLAY TABS
    public void GeneralPatternTab(ref StarPatternGenerator editorTarget)
    {
        Undo.RecordObject(editorTarget, "Edit StarPatternGenerator pattern parameters.");

        editorTarget.r1 = EditorGUILayout.IntField("N", editorTarget.r1);
        editorTarget.r2 = EditorGUILayout.IntField("D", editorTarget.r2);

        switch (editorTarget.pattern)
        {
            case Pattern.Epitrocloid:
            case Pattern.Hypotrocloid:
            case Pattern.Lissajous:
            //case Pattern.Rose:
                editorTarget.d = EditorGUILayout.FloatField("Distance", editorTarget.d);
                break;
        }
    }
    public void FarrisWheelTab(ref StarPatternGenerator editorTarget)
    {
        Undo.RecordObject(editorTarget, "Edit StarPatternGenerator Farris Wheel parameter(s).");
        /*/
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("");
        GUILayout.Label("1");
        GUILayout.Label("2");
        GUILayout.Label("3");
        EditorGUILayout.EndHorizontal();
        //*/

        //EditorGUILayout.BeginHorizontal();
        //EditorGUILayout.LabelField("W");
        editorTarget.w1 = EditorGUILayout.FloatField("W1", editorTarget.w1);
        editorTarget.w2 = EditorGUILayout.FloatField("W2", editorTarget.w2);
        editorTarget.w3 = EditorGUILayout.FloatField("W3", editorTarget.w3);
        //EditorGUILayout.EndHorizontal();
        EditorGUILayout.Separator();
        //EditorGUILayout.BeginHorizontal();
        //EditorGUILayout.LabelField("F");
        editorTarget.f1 = EditorGUILayout.FloatField("F1", editorTarget.f1);
        editorTarget.f2 = EditorGUILayout.FloatField("F2", editorTarget.f2);
        editorTarget.f3 = EditorGUILayout.FloatField("F3", editorTarget.f3);
        //EditorGUILayout.EndHorizontal();
        EditorGUILayout.Separator();
        //EditorGUILayout.BeginHorizontal();
        //EditorGUILayout.LabelField("P (Phase)");
        editorTarget.p1 = EditorGUILayout.FloatField("P1", editorTarget.p1);
        editorTarget.p2 = EditorGUILayout.FloatField("P2", editorTarget.p2);
        editorTarget.p3 = EditorGUILayout.FloatField("P3", editorTarget.p3);
        //EditorGUILayout.EndHorizontal();
    }
    //
    public void DebugInformationTab(ref StarPatternGenerator editorTarget)
    {
        Undo.RecordObject(editorTarget, "Edit StarPatternGenerator debug information.");

        GUIContent DebugInfoContent = new GUIContent("Debug Information".ToUpper(), "The resolution of the debug path renderer. Higher number is more efficient but less detailed.");
        editorTarget.foldout[(int)Display.DebugInformation] = EditorGUILayout.Foldout(editorTarget.foldout[(int)Display.DebugInformation], DebugInfoContent);
        if (editorTarget.foldout[(int)Display.DebugInformation])
        {
            editorTarget.debugColor = EditorGUILayout.ColorField("Debug Color", editorTarget.debugColor);
            editorTarget.debugFidelity = EditorGUILayout.Slider("Debug Resolution", editorTarget.debugFidelity, 0.05f, 4f);
            editorTarget.debugPositionSize = EditorGUILayout.FloatField("Debug Position Size", editorTarget.debugPositionSize);
            editorTarget.displayDebugGizmos = EditorGUILayout.Toggle("Display Gizmos", editorTarget.displayDebugGizmos);
        }
    }
    public void RotationTab(ref StarPatternGenerator editorTarget)
    {
        Undo.RecordObject(editorTarget, "Edit StarPatternGenerator rotation parameters.");

        editorTarget.foldout[(int)Display.Rotation] = EditorGUILayout.Foldout(editorTarget.foldout[(int)Display.Rotation], "Rotation".ToUpper());
        if (editorTarget.foldout[(int)Display.Rotation])
        {
            if (editorTarget.uniformScaling)
                editorTarget.preScaleRotation = EditorGUILayout.Slider("Rotate Pattern", editorTarget.preScaleRotation, 0f, 360f);
            else
            {
                editorTarget.preScaleRotation = EditorGUILayout.Slider("Rotate Pattern (Pre Scale)", editorTarget.preScaleRotation, 0f, 360f);
                editorTarget.postScaleRotation = EditorGUILayout.Slider("Rotate Pattern (Post Scale)", editorTarget.postScaleRotation, 0f, 360f);
            }
            editorTarget.invertCycleDirection = EditorGUILayout.Toggle("Invert Cycle", editorTarget.invertCycleDirection);
        }
    }
    public void ScaleTab(ref StarPatternGenerator editorTarget)
    {
        Undo.RecordObject(editorTarget, "Edit StarPatternGenerator rotation parameters.");

        editorTarget.foldout[(int)Display.Scale] = EditorGUILayout.Foldout(editorTarget.foldout[(int)Display.Scale], "Scale".ToUpper());
        if (editorTarget.foldout[(int)Display.Scale])
        {
            editorTarget.uniformScaling = EditorGUILayout.Toggle("Uniform Scaling?", editorTarget.uniformScaling);
            if (editorTarget.uniformScaling)
            {
                editorTarget.uniformScale = EditorGUILayout.FloatField("Scale", editorTarget.uniformScale);
                editorTarget.invertAxisX = EditorGUILayout.Toggle("Invert X", editorTarget.invertAxisX);
                editorTarget.invertAxisY = EditorGUILayout.Toggle("Invert Y", editorTarget.invertAxisY);
            }
            else
            {
                editorTarget.nonUniformScale = EditorGUILayout.Vector2Field("Scale", editorTarget.nonUniformScale);
            }
            //editorTarget.scaleBeforeRotation = EditorGUILayout.Toggle(new GUIContent("Scale Before Rotation", "Scales before applying rotation matrix."), editorTarget.scaleBeforeRotation);
        }
    }
    public void SatellitesTab(ref StarPatternGenerator editorTarget)
    {
        Undo.RecordObject(editorTarget, "Edit StarPatternGenerator satellite parameters.");

        editorTarget.foldout[(int)Display.Satellites] = EditorGUILayout.Foldout(editorTarget.foldout[(int)Display.Satellites], "Satellites".ToUpper());
        if (editorTarget.foldout[(int)Display.Satellites])
        {
            if (editorTarget.satellites.Count < 1)
                DefaultButton(editorTarget);
            else
            {

                SpaceSatellitesEvenlyButton(editorTarget);

                for (int i = 0; i < editorTarget.satellites.Count; i++)
                {
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label((i + 1).ToString());
                    editorTarget.satellites[i].transform = EditorGUILayout.ObjectField(editorTarget.satellites[i].transform, typeof(Transform), true) as Transform;
                    editorTarget.satellites[i].phase = EditorGUILayout.Slider(editorTarget.satellites[i].phase, 0f, 1f * editorTarget.Cycles);
                    AllButtons(editorTarget, i);
                    EditorGUILayout.EndHorizontal();
                }
            }
        }
    }

    // BUTTONS
    public void DefaultButton(StarPatternGenerator editorTarget)
    {
        if (editorTarget.satellites.Count < 1)
        {
            AddButton(editorTarget, -1);
        }
    }
    public void AllButtons(StarPatternGenerator editorTarget, int i)
    {
        MoveUpButton(editorTarget, i);
        MoveDownButton(editorTarget, i);
        AddButton(editorTarget, i);
        RemoveButton(editorTarget, i);
    }
    //
    public void AddButton(StarPatternGenerator editorTarget, int i)
    {
        Undo.RecordObject(editorTarget, "Add StarPatternGenerator satellite.");

        if (GUILayout.Button("+", GUILayout.Width(20f)))
        {
            Undo.RecordObject(editorTarget, "Add new node at index: " + i);
            editorTarget.Add(i + 1);
        }
    }
    public void RemoveButton(StarPatternGenerator editorTarget, int i)
    {
        Undo.RecordObject(editorTarget, "Remove StarPatternGenerator satellite.");

        if (GUILayout.Button("-", GUILayout.Width(20f)))
        {
            Undo.RecordObject(editorTarget, "Remove node at index: " + i);
            editorTarget.Remove(i);
        }
    }
    public void MoveDownButton(StarPatternGenerator editorTarget, int i)
    {
        Undo.RecordObject(editorTarget, "Move StarPatternGenerator satellite down.");

        if (GUILayout.Button("\u25bc".ToString(), GUILayout.Width(20f)))
        {
            Undo.RecordObject(editorTarget, "Move down.");
            editorTarget.MoveIndex(i, Mathf.Clamp(i + 2, 0, editorTarget.satellites.Count));
        }

    }
    public void MoveUpButton(StarPatternGenerator editorTarget, int i)
    {
        Undo.RecordObject(editorTarget, "Move StarPatternGenerator satellite up.");

        if (GUILayout.Button("\u25b2", GUILayout.Width(20f)))
        {
            Undo.RecordObject(editorTarget, "Move up.");
            editorTarget.MoveIndex(i, Mathf.Clamp(i - 1, 0, editorTarget.satellites.Count));
        }
    }
    //
    public void SpaceSatellitesEvenlyButton(StarPatternGenerator editorTarget)
    {
        Undo.RecordObject(editorTarget, "Align StarPatternGenerator satellites.");

        if (GUILayout.Button("Space Evenly"))
        {
            for (int index = 0; index < editorTarget.satellites.Count; index++)
            {
                editorTarget.satellites[index].phase = index * editorTarget.Cycles / editorTarget.satellites.Count;
                //editorTarget.satellite[index].transform.position = editorTarget.GetPosition(0f, editorTarget.satellite[index].phase);
                //editorTarget.UpdateSatellitePositions();
            }
        }
    }

    /// <summary>
    /// Returns a bool indicating if the tab is open or not (true / false).
    /// </summary>
    /// <param name="editorTarget"></param>
    /// <param name="display"></param>
    /// <returns></returns>
    public bool Foldout(StarPatternGenerator editorTarget, Display display)
    {
        return (editorTarget.foldout[(int)display]);
    }
}
