﻿//http://catlikecoding.com/unity/tutorials/editor/custom-data/

using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(Bool3))]
public class Bool3PropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect xyz, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(xyz, label, property);

        Rect contentPosition = EditorGUI.PrefixLabel(xyz, label);
        contentPosition.width /= 6f; // 6 because 1 labelfield and 1 propertyfield used 3 times

        BoolLabel("x", ref contentPosition, property, 3f);
        BoolLabel("y", ref contentPosition, property, 3f);
        BoolLabel("z", ref contentPosition, property, 3f);

        EditorGUI.EndProperty();
    }

    private static void BoolLabel(string propertyName, ref Rect contentRect, SerializedProperty property, float widthModifier)
    {
        EditorGUI.LabelField(contentRect, propertyName.ToUpper());
        contentRect.x += contentRect.width / widthModifier;
        EditorGUI.PropertyField(contentRect, property.FindPropertyRelative(propertyName), GUIContent.none);
        contentRect.x += contentRect.width / widthModifier * 2f;
    }
}