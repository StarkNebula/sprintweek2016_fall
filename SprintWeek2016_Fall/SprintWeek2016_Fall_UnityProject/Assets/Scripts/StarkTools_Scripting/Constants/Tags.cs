﻿public static partial class Tags
{
    // TAGS
    public const string Untagged = "Untagged";
    public const string Respawn = "Respawn";
    public const string Finish = "Finish";
    public const string EditorOnly = "EditorOnly";
    public const string MainCamera = "MainCamera";
    public const string Player = "Player";
    public const string GameController = "GameController";

    // LAYERS
    public const string Default = "Default";
    public const string TransparentFX = "TransparentFX";
    public const string IgnoreRaycast = "Ignore Raycast";
    public const string Water = "Water";
    public const string UI = "UI";
}