﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public struct PriorityDelegate
{
    public int priorityLevel;
    public Action delegateAction;
}

public class DelegateSystem : MonoBehaviour
{
    Queue<PriorityDelegate> UpdateDelegates;
    Queue<PriorityDelegate> FixedUpdateDelegates;

    public void AssessForUpdsate(int priority, Action method)
    {

    }

    private void Update()
    {
        while (UpdateDelegates.Count > 0)
            UpdateDelegates.Dequeue();
    }
    private void FixedUpdate()
    {
        while (FixedUpdateDelegates.Count > 0)
            FixedUpdateDelegates.Dequeue();
    }

}