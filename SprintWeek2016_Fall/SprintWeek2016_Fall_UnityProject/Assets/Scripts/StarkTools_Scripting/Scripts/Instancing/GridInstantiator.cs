﻿using UnityEngine;

public class GridInstantiator : MonoBehaviour
{
    public GameObject instance;
    public Color gizmosColor = Palette.white.SetAlpha(0.7f);
    public bool debugPlacement;

    public Vector3 gridSize = Vector3.one;
    public Vector3 padding;
    public float jitterRange = 0f;

    /// <summary>
    /// Generates a 3D grid of objects
    /// </summary>
    public void Generate()
    {
        for (int i = 0; i < gridSize.x; i++)
        {
            for (int j = 0; j < gridSize.y; j++)
            {
                for (int k = 0; k < gridSize.z; k++)
                {
                    GameObject go = (GameObject)Instantiate(instance, transform.position + CalculatePosition(i, j, k), Quaternion.identity);
                    go.name = instance.name + "_x" + i.ToString("D3") + "_y" + j.ToString("D3") + "_z" + k.ToString("D3");
                    go.transform.parent = this.transform;
                }
            }
        }
    }

    /// <summary>
    /// Generates a random float between negative and positive jitterRange.
    /// </summary>
    private float GenerateJitter()
    {
        return Random.Range(-jitterRange, jitterRange);
    }
    private Vector3 CalculatePosition(int i, int j, int k)
    {
        return 
           transform.forward * (i + GenerateJitter()) + (transform.forward * padding.y * i) +
           transform.right   * (j + GenerateJitter()) + (transform.right   * padding.x * j) +
           transform.up      * (k + GenerateJitter()) + (transform.up      * padding.z * k);
    }

    // Debug
    private void OnDrawGizmos()
    {
        if (debugPlacement)
        {
            Gizmos.color = gizmosColor;
            Mesh m = instance.GetComponent<MeshFilter>().sharedMesh;

            for (int i = 0; i < gridSize.x; i++)
                for (int j = 0; j < gridSize.y; j++)
                    for (int k = 0; k < gridSize.z; k++)
                        Gizmos.DrawMesh(m, transform.position + CalculatePosition(i, j, k), Quaternion.identity);
        }
    }
}