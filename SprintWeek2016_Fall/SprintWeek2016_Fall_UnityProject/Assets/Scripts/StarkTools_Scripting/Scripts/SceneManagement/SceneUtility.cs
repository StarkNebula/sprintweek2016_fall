﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneUtility : MonoBehaviour
{
    /// <summary>
    /// Loads the scene named <paramref name="sceneName"/>.
    /// </summary>
    /// <param name="sceneName"></param>
    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
    /// <summary>
    /// Loads the scene named <paramref name="sceneName"/>.
    /// </summary>
    /// <param name="sceneName"></param>
    public static void LoadSceneStatic(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    /// <summary>
    /// Loads the <paramref name="sceneObject"/> by name.
    /// </summary>
    /// <param name="sceneName"></param>
    public void LoadScene(Object sceneObject)
    {
        SceneManager.LoadScene(sceneObject.name);
    }
    /// <summary>
    /// Loads the <paramref name="sceneObject"/> by name.
    /// </summary>
    /// <param name="sceneName"></param>
    public static void LoadSceneStatic(Object sceneObject)
    {
        SceneManager.LoadScene(sceneObject.name);
    }

    /// <summary>
    /// Reloads the current stage.
    /// </summary>
    public void HardReloadCurrentScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    /// <summary>
    /// Quits the application.
    /// </summary>
    public void Quit()
    {
        Application.Quit();
    }
}