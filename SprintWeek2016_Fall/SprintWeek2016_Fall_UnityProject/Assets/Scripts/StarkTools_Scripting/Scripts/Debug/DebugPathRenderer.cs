﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DebugPathRenderer : MonoBehaviour
{
    // Each stored position
    private List<Vector3> points = new List<Vector3>();
    // Parameters
    public Color color = Color.white;
    public float minDistance = 0.01f;
    public int maxPositions = 60*60*10; // 60 frames * 60 seconds * 10 minutes

    void Start()
    {
        // null
        points.Add(this.transform.position);
        // Capture first position to reference, then again to avoid error
        points.Add(this.transform.position);
    }

	void FixedUpdate ()
    {
        // If distance from last position is greater than [var:minDistance]
        if ((points[points.Count - 1] - this.transform.position).magnitude > minDistance)
            points.Add(this.transform.position);

        // Draw between all points in list (except the very first)
        for (int i = 1; i < points.Count - 1; i++)
            Debug.DrawLine(points[i], points[Mathf.Max((i + 1) % points.Count, 0)], color);

        if (points.Count > maxPositions)
            points.RemoveAt(0);

        // Draw line from last storeposition to current position
        // Makes debug line seamless as a position not store may leave a gap until one is
        Debug.DrawLine(this.transform.position, points[points.Count - 1], color);
	}

}
