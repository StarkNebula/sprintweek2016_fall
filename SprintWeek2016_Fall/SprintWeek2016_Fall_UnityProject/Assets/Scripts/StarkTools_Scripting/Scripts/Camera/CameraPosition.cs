﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraPosition : MonoBehaviour
{

    public float guiAlpha = 1f;
    public int currentPosition = 0;
    public bool isLerping = false;
    public float lerpTime = 1f;

    public Transform DebugPositions;
    public List<Transform> trans;
    public List<float> Fov;
    public List<Rect> viewport;

    new public Camera camera;
    public AnimationCurve animationCurve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

    //
    private Vector3 defaultPosition;
    //public CameraShake cameraShake;

    void Start()
    {
        camera = this.GetComponent<Camera>();
        //cameraShake = this.gameObject.AddComponent<CameraShake>();
    }

    //
    void FixedUpdate()
    {
        if (!isLerping)
            defaultPosition = trans[currentPosition].position;

        //camera.transform.position = defaultPosition + cameraShake.screenshake;
    }

    public void DoScreenshake(float duration, float intensity)
    {
        //cameraShake.shakeIntensity = intensity;
        //StartCoroutine(cameraShake.Screenshake(duration));
    }

    public IEnumerator LerpToPosition(int nextPosition)
    {
        isLerping = true;
        float lerp = 0f;

        while (lerp < 1f)
        {
            // Increment Lerp
            lerp += Time.deltaTime / lerpTime;

            // Position Lerp
            //camera.transform.position = Vector3.Lerp(trans[currentPosition].position, trans[nextPosition].position, animationCurve.Evaluate(lerp));

            // Temp
            defaultPosition = Vector3.Lerp(trans[currentPosition].position, trans[nextPosition].position, animationCurve.Evaluate(lerp));

            // Rotation Lerp
            camera.transform.rotation = Quaternion.Lerp(trans[currentPosition].rotation, trans[nextPosition].rotation, animationCurve.Evaluate(lerp));
            // FOV Lerp
            camera.fieldOfView = Mathf.Lerp(Fov[currentPosition], Fov[nextPosition], animationCurve.Evaluate(lerp));
            //
            camera.rect = new Rect(
                Mathf.Lerp(viewport[currentPosition].x, viewport[nextPosition].x, animationCurve.Evaluate(lerp)),
                Mathf.Lerp(viewport[currentPosition].y, viewport[nextPosition].y, animationCurve.Evaluate(lerp)),
                Mathf.Lerp(viewport[currentPosition].width, viewport[nextPosition].width, animationCurve.Evaluate(lerp)),
                Mathf.Lerp(viewport[currentPosition].height, viewport[nextPosition].height, animationCurve.Evaluate(lerp)));

            yield return new WaitForFixedUpdate();
        }

        currentPosition = nextPosition;
        isLerping = false;
    }

    public void RecordPosition()
    {
        GameObject empty = new GameObject();
        empty.transform.position = camera.transform.position;
        empty.transform.rotation = camera.transform.rotation;
        empty.transform.parent = DebugPositions;
        empty.name = "WayPoint_" + DebugPositions.childCount;


  
        trans.Add(empty.transform);

        //
        Fov.Add(camera.fieldOfView);
        viewport.Add(camera.rect);
    }

    public void DeletePosition(int positionToDelete)
    {
        trans.RemoveAt(positionToDelete);
        DestroyImmediate(DebugPositions.GetChild(positionToDelete).gameObject);

        Fov.RemoveAt(positionToDelete);
        viewport.RemoveAt(positionToDelete);

        for (int i = 0; i < DebugPositions.childCount; i++)
            DebugPositions.GetChild(i).name = "WayPoint_" + (i + 1).ToString();
    }

    public void WarpToPosition(int i)
    {
        camera.transform.position = trans[i].position;
        camera.transform.rotation = trans[i].rotation;
        camera.fieldOfView = Fov[i];
        camera.rect = viewport[i];
        currentPosition = i;
    }

    public void OnDrawGizmos()
    {
        for (int i = 0; i < trans.Count; i++)
        {
            //Gizmos.color = Palette.SetAlpha(Palette.ColourWheel(Palette.Wheel.Tertiary, Palette.GenericColours, i), guiAlpha);
            Gizmos.color = Palette.SetAlpha(Palette.ColorWheel(i, 24), guiAlpha);
            Gizmos.DrawSphere(trans[i].position, 1f);
            Debug.DrawLine(trans[i].position, trans[i].position + DebugPositions.GetChild(i).transform.right, Palette.red_orange);
            Debug.DrawLine(trans[i].position, trans[i].position + DebugPositions.GetChild(i).transform.up, Palette.lime_green);
            Debug.DrawLine(trans[i].position, trans[i].position + DebugPositions.GetChild(i).transform.forward, Palette.cobalt);
        }
    }

    public void Test()
    {
        if (!isLerping)
        {
            //DoScreenshake(1f, 3f);
            StartCoroutine(LerpToPosition((currentPosition + 1) % trans.Count));
        }
    }

}
