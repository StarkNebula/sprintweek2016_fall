﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    [Range(0f, .25f)]
    public float lerpValue = 0.05f;
    public Transform target;
    public Vector3 offset = new Vector3();

    void Awake()
    {
        this.transform.position = target.transform.position + offset;
    }

    void FixedUpdate()
    {
        this.transform.position = Vector3.Lerp(this.transform.position, target.transform.position + offset, lerpValue);

        Quaternion rotA = this.transform.rotation;
        this.transform.LookAt(target.transform.position);
        Quaternion rotB = this.transform.rotation;
        this.transform.rotation = Quaternion.Lerp(rotA, rotB, lerpValue);
    }
}
