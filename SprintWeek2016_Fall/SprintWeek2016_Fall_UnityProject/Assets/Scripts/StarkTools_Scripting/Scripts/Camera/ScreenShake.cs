﻿using UnityEngine;
using System.Collections;

public class ScreenShake : MonoBehaviour
{
    public Bool3 targetAxes;
    public bool useGlobalAxes;

    new public Camera camera;

    // Lookat target
    private Vector3 position;
    public Transform lookAtTarget;

    // OnCollisionEnter vars
    private Vector3 screenShake;
    public float screenShakeDuration = 5f;
    public float screenShakeDistanceMultiplier = 3f;


    void Start()
    {
        if (camera == null)
            camera = GetComponent<Camera>();
    }


    void FixedUpdate()
    {
        Vector3 targetPosition = new Vector3(
            (targetAxes.x) ? lookAtTarget.transform.position.x : 0f,
            (targetAxes.y) ? lookAtTarget.transform.position.y : 0f,
            (targetAxes.z) ? lookAtTarget.transform.position.z : 0f);


        // Add any screenshake
        camera.transform.position = position + screenShake;
        transform.LookAt(targetPosition, Vector3.up);

        // Record real position
        position = camera.transform.position - screenShake;
    }


    public IEnumerator Screenshake()
    {
        float timestamp = Time.time;

        while (Time.time - timestamp < screenShakeDuration)
        {
            float shakeRange = (screenShakeDuration - (Time.time - timestamp)) * screenShakeDistanceMultiplier;

            screenShake =
                Random.Range(-shakeRange, shakeRange) * (useGlobalAxes ? Vector3.right   : transform.right  ) +
                Random.Range(-shakeRange, shakeRange) * (useGlobalAxes ? Vector3.up      : transform.up     ) +
                Random.Range(-shakeRange, shakeRange) * (useGlobalAxes ? Vector3.forward : transform.forward);

            yield return new WaitForFixedUpdate();
        }

        // After loop, reset to zero (Stop)
        screenShake = Vector3.zero;
    }
}
