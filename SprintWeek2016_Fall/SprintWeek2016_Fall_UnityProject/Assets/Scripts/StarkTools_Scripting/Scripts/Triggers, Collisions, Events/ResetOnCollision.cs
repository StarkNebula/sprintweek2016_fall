﻿using UnityEngine;

public class ResetOnCollision : MonoBehaviour {

    public Transform target;
    public string targetTag = "Player";
    public bool resetScale;

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == targetTag)
            ResetTransformToTarget(collider.transform);
    }
    void OnTriggerEnter(Collision collision)
    {
        if (collision.gameObject.tag == targetTag)
            ResetTransformToTarget(collision.transform);
    }

    void ResetTransformToTarget(Transform t)
    {
            t.position = target.position;
            t.rotation = target.rotation;
            if (resetScale)
                t.localScale = target.localScale;
    }

}
