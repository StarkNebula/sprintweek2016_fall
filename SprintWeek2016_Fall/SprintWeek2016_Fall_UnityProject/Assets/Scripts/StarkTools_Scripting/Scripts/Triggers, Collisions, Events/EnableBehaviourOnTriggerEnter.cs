﻿using UnityEngine;

public class EnableBehaviourOnTriggerEnter : MonoBehaviour {

    // Behaviours are components/classes
    public Behaviour[] behaviour;
    public bool turnOffAfterScriptRuns = true;

    void Start()
    {
        // Turn behaviours off
        foreach (Behaviour component in behaviour)
            if (component.enabled == true)
                component.enabled = false;
    }

    void OnTriggerEnter()
    {
        // Turn behaviours on
        foreach (Behaviour component in behaviour)
            component.enabled = true;

        // Disable script
        if (turnOffAfterScriptRuns)
            this.enabled = false;
    }
}
