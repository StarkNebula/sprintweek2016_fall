﻿namespace UnityEngine
{
    public class KillCollider : MonoBehaviour
    {
        void OnTriggerEnter(Collider collider)
        {
            Destroy(collider.gameObject);
        }

        void OnCollisionEnter(Collision collision)
        {
            Destroy(collision.gameObject);
        }
    }
}