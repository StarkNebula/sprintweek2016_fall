﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class UnityEventCollider : UnityEvent<Collider> { }


public class TriggerEvents : MonoBehaviour
{
    public float onTriggerStayFrequency = 1f;
    private float lastOnTriggerStay;

    public UnityEventCollider onTriggerEnter;
    public UnityEventCollider onTriggerStay;
    public UnityEventCollider onTriggerExit;


    public void OnTriggerEnter(Collider collider)
    {
        onTriggerEnter.Invoke(collider);
        lastOnTriggerStay = Time.time;
    }


    public void OnTriggerStay(Collider collider)
    {
        if (Time.time - lastOnTriggerStay > onTriggerStayFrequency)
        {
            onTriggerEnter.Invoke(collider);
            lastOnTriggerStay = Time.time;
        }
    }

    public void OnTriggerExit(Collider collider)
    {
        onTriggerExit.Invoke(collider);
    }

}
