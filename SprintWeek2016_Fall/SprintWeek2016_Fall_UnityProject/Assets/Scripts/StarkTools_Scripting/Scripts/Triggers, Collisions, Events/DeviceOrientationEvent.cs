﻿namespace UnityEngine
{
    [System.Serializable]
    public class DeviceOrientationChangedEvent : Events.UnityEvent<DeviceOrientation> { }

    public class DeviceOrientationEvent : MonoBehaviour
    {
        public static DeviceOrientation deviceOrientation;
        public DeviceOrientationChangedEvent onDeviceOrientationChangedEvent;

        void Awake()
        {
            deviceOrientation = Input.deviceOrientation;
        }

        void FixedUpdate()
        {
            if (deviceOrientation != Input.deviceOrientation)
            {
                onDeviceOrientationChangedEvent.Invoke(Input.deviceOrientation);
                deviceOrientation = Input.deviceOrientation;
            }
        }
    }
}