﻿using UnityEngine;
using System.Collections.Generic;

public class WarpTo : MonoBehaviour {

    public float guiAlpha = 1f;
    public float guiSphereRadius;
    public List<TransformStruct> transforms = new List<TransformStruct>();


    void OnDrawGizmos()
    {
        for (int i = 0; i < transforms.Count; i++)
        {
            Gizmos.color = Palette.ColorWheel(i, 6).Blacken(.5f).SetAlpha(guiAlpha);
            Gizmos.DrawSphere(transforms[i].position, guiSphereRadius);
        }
    }

    public void WarpToPosition(int index)
    {
        transform.position = transforms[index].position;
        transform.rotation = transforms[index].rotation;
    }
    public void RecordPosition()
    {
        transforms.Add(new TransformStruct(transform.position, transform.rotation, transform.localScale));
    }
    public void RecordPosition(int index)
    {
        transforms.Insert(index, new TransformStruct(transform.position, transform.rotation, transform.localScale));
    }
    public void DeletePosition(int index)
    {
        transforms.RemoveAt(index);
    }
    public void MoveIndex(int index, int newIndex)
    {
        TransformStruct t = transforms[index];

        transforms.Insert(newIndex, t);
        transforms.RemoveAt( (index < newIndex) ? index : index + 1 );
    }
}
