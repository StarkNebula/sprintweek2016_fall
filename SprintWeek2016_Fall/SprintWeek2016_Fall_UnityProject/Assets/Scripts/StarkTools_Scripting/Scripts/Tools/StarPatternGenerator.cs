﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class StarPatternGenerator : MonoBehaviour
{
    #region VARIABLES - Display, Editor, Patterns

    public Pattern pattern;
    public List<Satellite> satellites = new List<Satellite>();
    public Vector2 offset;

    // Display
    public Color debugColor = Color.red;
    public float debugFidelity = 1.5f;
    public float debugPositionSize = 0.5f;
    public bool displayDebugGizmos = true;

    // Editor parameters
    public float preScaleRotation;
    public float postScaleRotation;
    public bool invertAxisX;
    public bool invertAxisY;
    public bool invertCycleDirection;
    //public bool normalizeScale;
    //public bool scaleBeforeRotation = true;
    public bool uniformScaling = true;
    public float uniformScale = 1f;
    public Vector2 nonUniformScale = Vector2.one;
    public float secondsPerCycle = 15f;

    // Editor Display
    //public bool[] foldout = ArrayExtensions.NewArray(EnumExtensions.Length(typeof(Display)), true);
    public bool[] foldout = { true, true, true, true, true };
    //public bool[] foldout = new bool[]();


    // Variables for curve patterns, initialized
    public int r1 = 2;
    public int r2 = 3;
    public float d = 1f;
    public float angle;
    public float w1 = 1.33f;
    public float w2 = 0.50f;
    public float w3 = 0.33f;
    public float f1 = 6f;
    public float f2 = 5f;
    public float f3 = -17f;
    public float p1, p2, p3;
    public float Cycles { get
        {
            switch (pattern)
            {
                case Pattern.FarrisWheel:
                case Pattern.Lissajous:
                    return 1f;
                default:
                    return (r2 == 0) ? 1 : (r2 / MathX.GreatestCommonDivisor(r1, r2));
            }
        }
    }
    #endregion

    public void FixedUpdate()
    {
        UpdateSatellitePositions();
    }


    public void UpdateSatellitePositions()
    {
        // Update the position of each satellite / planetoid
        foreach (Satellite s in satellites)
        {
            if (s.transform != null)
                s.transform.position = GetPositionInCycle(Time.time * Cycles, s.phase);
        }
    }
    /// <summary>
    /// Returns the appropriate pattern depending on the enum/menu.
    /// The public variables are plugged in directly to this method.
    /// </summary>
    /// <param name="_pattern">The Pattern enum.</param>
    public Vector2 GetPattern (Pattern _pattern, float time)
    {
        switch (_pattern)
        {
            case Pattern.Epicycloid:
                return ParametricPatterns.Epicycloid(time, r1, r2);

            case Pattern.Epitrocloid:
                return ParametricPatterns.Epitrochoid(time, r1, r2, d);

            case Pattern.Hypocycloid:
                return ParametricPatterns.Hypocycloid(time, r1, r2);

            case Pattern.Hypotrocloid:
                return ParametricPatterns.Hypotrochoid(time, r1, r2, d);

            case Pattern.FarrisWheel:
                return ParametricPatterns.FarrisWheel(time, w1, w2, w3, f1, f2, f3, p1, p2, p3);

            case Pattern.Lissajous:
                return ParametricPatterns.Lissajous(time, angle, r1, r2);

            case Pattern.Rose:
                return ParametricPatterns.Rose(time, r1, r2);

            default:
                throw new ArgumentException();
        }
    }
    /// <summary>
    /// Note: hardcoded.
    /// This method could use some reworking and clarification.
    /// </summary>
    /// <param name="cyclePointInPercent">The cycle point in degrees.</param>
    /// <returns></returns>
    public Vector2 GetPosition(float cyclePointInPercent)
    {
        // Invert time, makes Clockwise -> CounterClockwise
        if (invertCycleDirection)
            cyclePointInPercent = -cyclePointInPercent;

        // Transform
        Vector2 position = GetPattern(pattern, cyclePointInPercent);

        position = ApplyRotation(position, preScaleRotation);
        position = ApplyScale(position);
        position = ApplyRotation(position, postScaleRotation);

        // Allow inverse scaling. You can set X or Y to negative to achieve
        // this effect but not when scaling is uniform.
        position = position.Mirror(invertAxisX, invertAxisY);

        return position;
    }
    public Vector2 GetPositionInCycle(float time, float phase)
    {
        return GetPosition((time / secondsPerCycle + phase) * 360f) + (Vector2)transform.position + offset;
    }
    /// <summary>
    /// Applies a Z-Rotation matrice to the Vector2 using an <paramref name="angle"/> in degrees.
    /// </summary>
    /// <param name="vector">The Vector2 to rotate.</param>
    /// <param name="angle">The angle in degrees.</param>
    /// <returns></returns>
    public Vector2 ApplyRotation(Vector2 vector, float angle)
    {
        if (angle % 360f == 0f)
            return vector;

        return new Vector2(
              MathX.Cos(angle) * vector.x - MathX.Sin(angle) * vector.y,
              MathX.Sin(angle) * vector.x + MathX.Cos(angle) * vector.y
            );
    }
    /// <summary>
    /// Note: hardcoded.
    /// </summary>
    /// <param name="vector"></param>
    /// <returns></returns>
    public Vector2 ApplyScale(Vector2 vector)
    {
        if (uniformScaling)
            vector *= uniformScale;
        else
            vector = new Vector2(vector.x * nonUniformScale.x, vector.y * nonUniformScale.y);

        return vector;
    }

    // DEBUG
    public void OnDrawGizmos()
    {
        if (displayDebugGizmos)
        {
            // Display pattern
            Gizmos.color = debugColor;
            Vector2 currentPosition = this.transform.position;
            for (float f = 0f; f < 360f * Cycles; f += debugFidelity)
                Gizmos.DrawLine(GetPosition(f) + currentPosition + offset, GetPosition(f + debugFidelity) + currentPosition + offset);

            // Display satellite positions
            Gizmos.color = Gizmos.color.Whitten(0.5f).SetAlpha(0.67f);
            foreach (Satellite s in satellites)
            {
                Gizmos.DrawSphere(GetPositionInCycle(0f, s.phase), debugPositionSize);
                Gizmos.DrawLine(transform.position, GetPositionInCycle(0f, s.phase));
            }
        }
    }

    // EDITOR
    // Used for managing list
    public void Add(int index)
    {
        satellites.Insert(index, new Satellite(this.transform));
    }
    public void Remove(int index)
    {
        satellites.RemoveAt(index);
    }
    public void MoveIndex(int index, int newIndex)
    {
        Satellite s = satellites[index];

        satellites.Insert(newIndex, s);
        satellites.RemoveAt((index < newIndex) ? index : index + 1); // Check to see if insert "bumped" index up
    }
}

/// <summary>
/// Enum list of all the possible patterns for the satellite to orbit with.
/// </summary>
public enum Pattern
{
    Epicycloid,
    Epitrocloid,
    FarrisWheel,
    Hypocycloid,
    Hypotrocloid,
    Lissajous,
    Rose,
}
/// <summary>
/// Foldout bool references for editor
/// </summary>
public enum Display
{
    DebugInformation,
    Rotation,
    Scale,
    Parameters,
    Satellites,
}

/// <summary>
/// Satellite: a celestial body orbiting the earth or another planet.
/// </summary>
[Serializable]
public class Satellite
{
    public float phase;
    public Transform transform;
    public Transform parent;
    // Preload all points into memory?
    //public Vector3[] points;

    public Satellite(Transform parent)
    {
        this.parent = parent;
    }
}