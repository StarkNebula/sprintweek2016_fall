﻿using UnityEngine;

public class AnimateTextureDeltaTime : MonoBehaviour {

    new private Renderer renderer;
    public Vector2 tiling;    

    void Start ()
    {
        renderer = GetComponent<Renderer>();
	}
	
	void FixedUpdate ()
    {
        Vector2 newOffset = renderer.material.mainTextureOffset;
        newOffset += tiling * Time.deltaTime;
        newOffset = new Vector2(newOffset.x % 1f, newOffset.y % 1f); 
        renderer.material.mainTextureOffset = newOffset;
    }
}
