﻿using UnityEngine;
using System.Collections;

public class MeshDebugger : MonoBehaviour
{

    public Mesh collisionMesh;
    public Color color = Palette.white.SetAlpha(.66f);
    public float scaleModifier = 1f;

    void OnDrawGizmos()
    {
        Gizmos.color = color;
        Gizmos.DrawMesh(
            collisionMesh,
            transform.position,
            transform.rotation,
            transform.lossyScale * scaleModifier
            );
    }

}
