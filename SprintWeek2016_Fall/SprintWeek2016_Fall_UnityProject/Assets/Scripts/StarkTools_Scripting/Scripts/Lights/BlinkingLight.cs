﻿using UnityEngine;

namespace StarkTools.Lights
{
    public class BlinkingLight : MonoBehaviour
    {
        public Light[] lights;

        public AnimationCurve blinkPattern;
        public float minTime;
        public float maxTime;

        public bool isOn;

        private float[] intensity;
        private float randomizedCycleTime;
        private float currentTimeInCycle;


        void Start()
        {
            intensity = new float[lights.Length];
            for (int i = 0; i < lights.Length; i++)
                intensity[i] = lights[i].intensity;

            RandomizeTime();
        }
        void FixedUpdate()
        {
            currentTimeInCycle += Time.deltaTime;

            if (isOn)
                for (int i = 0; i < lights.Length; i++)
                    lights[i].intensity = blinkPattern.Evaluate(currentTimeInCycle / randomizedCycleTime) * intensity[i];

            else // isOff
                foreach (Light light in lights)
                    light.intensity = 0f;

            if (currentTimeInCycle > randomizedCycleTime)
                RandomizeTime();
        }

        void RandomizeTime()
        {
            randomizedCycleTime = Random.Range(minTime, maxTime);
            currentTimeInCycle = 0f;
            isOn = !isOn; // 1 cycle on, one cycle off
        }
    }
}