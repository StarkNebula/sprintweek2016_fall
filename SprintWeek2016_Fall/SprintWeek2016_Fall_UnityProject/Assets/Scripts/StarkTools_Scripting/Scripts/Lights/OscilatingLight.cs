﻿using UnityEngine;

namespace StarkTools.Lights
{
    public class OscilatingLight : MonoBehaviour
    {
        new private Light light;
        public AnimationCurve animationCurve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);
        public float cycleTime = 1f;

        private float intensity;

        void Start()
        {
            light = GetComponent<Light>();
            intensity = light.intensity;
        }

        void FixedUpdate()
        {
            float currentTime = (Mathf.Cos((Time.time / cycleTime) * Mathf.Deg2Rad) + 1f) / 2f;

            // foreach (Light light in lights)
            light.intensity = animationCurve.Evaluate(currentTime) * intensity;
        }
    }
}