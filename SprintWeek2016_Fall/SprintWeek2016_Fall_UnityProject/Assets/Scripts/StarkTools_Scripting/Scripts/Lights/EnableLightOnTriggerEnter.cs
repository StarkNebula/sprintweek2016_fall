﻿using UnityEngine;
using System.Collections;

namespace StarkTools.Lights
{
    public class EnableLightOnTriggerEnter : MonoBehaviour
    {
        public string colliderTag = "Player";

        new public Light[] light;
        private float[] intensity;

        // Fading in
        public float attack = 1f;
        public AnimationCurve animationCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        private bool canBeActivated = true;
        public bool canBeReactivated;

        // Fading out
        public bool lightDiesOut;
        public float sustain = 1f;
        public float release = 1f;
        public AnimationCurve animationCurveOut = AnimationCurve.Linear(0f, 0f, 1f, 1f);

        void Start()
        {
            // Copies all the intensities of each light to array
            // Turns all intensities to 0f (off)
            intensity = new float[light.Length];
            for (int i = 0; i < light.Length; i++)
            {
                intensity[i] = light[i].intensity;
                light[i].intensity = 0f;
            }
        }

        void OnTriggerEnter(Collider collider)
        {
            if (canBeActivated)
                if (collider.tag == colliderTag)
                    StartCoroutine(TurnLightOn());
        }

        IEnumerator TurnLightOn()
        {
            canBeActivated = false;
            float currentTime = 0f;

            // Attack
            // While current time < 100%
            while (currentTime < 1f)
            {
                for (int i = 0; i < light.Length; i++)
                    light[i].intensity = animationCurve.Evaluate(currentTime) * intensity[i];

                currentTime += Time.deltaTime / attack;
                yield return new WaitForFixedUpdate();
            }

            if (lightDiesOut)
            {
                // Sustain light intensity for this length of time
                yield return new WaitForSeconds(sustain);

                // Release
                // While current time > 0%
                while (currentTime > 0f)
                {
                    for (int i = 0; i < light.Length; i++)
                        light[i].intensity = animationCurveOut.Evaluate(currentTime) * intensity[i];

                    currentTime -= Time.deltaTime / release;
                    yield return new WaitForFixedUpdate();
                }
            }

            if (canBeReactivated)
                canBeActivated = true;
        }
    }
}