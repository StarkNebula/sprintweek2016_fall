﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace StarkTools.Lights
{
    public class StageLightEffect : MonoBehaviour
    {
        public Light[] lights;
        private float[] intensity;
        public AnimationCurve curve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        public float attack = 0.2f;
        public float totalTime = 3f;

        private bool hasBeenActivated;
        public bool canRetriggerEvent;

        // Event fired when light turns on. Returns Light.
        // Get GameObject with light.gameobject
        public UnityEvent<Light> lightTurnedOn;


        void Start()
        {
            // Record intensities
            intensity = new float[lights.Length];
            // Turn all lights off
            for (int i = 0; i < lights.Length; i++)
            {
                intensity[i] = lights[i].intensity;
                lights[i].intensity = 0f;
            }
        }

        void OnTriggerEnter()
        {
            if (!hasBeenActivated)
            {
                for (int i = 0; i < lights.Length; i++)
                {
                    // Calculate delay based off of curve
                    float delay = (float)i / lights.ArrayLength();
                    StartCoroutine(TurnLightOn(i, curve.Evaluate(delay) * totalTime));
                }
            }
        }

        IEnumerator TurnLightOn(int i, float delay)
        {
            // Wait for delay
            yield return new WaitForSeconds(delay);

            // Return the light that is being enables
            // You can also get the GameObject with this via lights.gameobject;
            lightTurnedOn.Invoke(lights[i]);

            float currentTime = 0f; // 0% - 100%
            while (currentTime < attack)
            {
                currentTime += Time.deltaTime;
                lights[i].intensity = (currentTime / attack) * intensity[i];
                yield return new WaitForFixedUpdate();
            }

            if (canRetriggerEvent)
                hasBeenActivated = false;
        }

        // DEBUG
        //void Update()
        //{
        //    if (Input.GetKeyDown(KeyCode.Space))
        //        OnTriggerEnter();
        //}

    }
}