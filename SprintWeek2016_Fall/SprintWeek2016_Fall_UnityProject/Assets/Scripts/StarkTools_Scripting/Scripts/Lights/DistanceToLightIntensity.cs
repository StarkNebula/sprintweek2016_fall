﻿using UnityEngine;

namespace StarkTools.Lights
{
    public class DistanceToLightIntensity : MonoBehaviour
    {
        new Light light;

        public Transform transformToComapreTo;
        public float distance;
        public float distanceThreshold = 100f;
        public float minIntensity = 0f;
        private float maxIntensity;
        [Tooltip("Light stops dimming or brightening at this distance.")]
        public float cutoffDistance = 10f;

        public enum Approach
        {
            DimsLight,
            BrightensLight
        }
        public Approach approach = Approach.DimsLight;

        void Start()
        {
            light = GetComponent<Light>();
            maxIntensity = light.intensity;
        }

        void FixedUpdate()
        {
            distance = Vector3.Distance(this.transform.position, transformToComapreTo.position);

            if (approach == Approach.DimsLight)
                //(distance - cutoffDistance) = (x - 25) = range
                //(distanceThreshold - cutoffDistance) = 100 - 25 = range working with
                light.intensity = Mathf.Clamp(((distance - cutoffDistance) / (distanceThreshold - cutoffDistance)) * maxIntensity, minIntensity, maxIntensity);
            else // if (approach == Approach.BrightensLight)
                light.intensity = Mathf.Clamp((distanceThreshold - distance) * maxIntensity, minIntensity, maxIntensity);
        }
    }
}