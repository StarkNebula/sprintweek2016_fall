﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class SplinePath : MonoBehaviour
{
	public bool isDotted;
	public bool isLooping;
	private bool[] hasFinished;

	public Transform[] keyPosition;
	public float[] lengthPerSegment;
	public float entireCycleTime = 10f;



	public float pathLength = 0f;

	public List<Rigidbody> rb;
	public float steps = 20f;
	public List<int> currentSegment;// = 0;
	public List<float> currentStep;// = 0;
	public Vector3 interpolatedPosition;



	void Start()
	{
		// Get references
		keyPosition = new Transform[this.transform.childCount];

		for (int i = 0; i < keyPosition.Length; i++)
			keyPosition[i] = this.transform.GetChild(i);

		// If can interpolate
		if (keyPosition.Length > 1)
		{
			// Set array size
			lengthPerSegment = new float[keyPosition.Length];

			int temp = (isLooping) ? 0 : 1;

			// For each Key Position
			for (int i = 0; i < keyPosition.Length - temp; i++)
			{
				// For each interval of var:steps
				for (float j = 0; j < steps; j++)
				{
					lengthPerSegment[i] += (HermiteSpline(i, j) - HermiteSpline(i, (j + 1))).magnitude;
				}
			}

            pathLength = 0;
			for (int i = 0; i < lengthPerSegment.Length; i++)
				pathLength += lengthPerSegment[i];

			//Debug.Log("Estimate path length: " + pathLength);
			//Debug.Break();

			hasFinished = new bool[rb.Count];
			//currentSegment = new int[rb.Length];
			//currentStep = new float[rb.Length];
		}
	}

	void FixedUpdate()
	{
		for (int i = 0; i < rb.Count; i++)
		{
			if (isLooping)
				Loop(i);
			else
				Line(i);
		}
	}

	// Debug
	void Update()
	{
		// Only while NOT in Play mode
		if (!Application.isPlaying)
		{
			//
			keyPosition = new Transform[this.transform.childCount];

			for (int i = 0; i < keyPosition.Length; i++)
				keyPosition[i] = this.transform.GetChild(i);

		}

		if (keyPosition.Length > 1)
		{
			int temp = (isLooping) ? 0 : 1;

			for (int i = 0; i < keyPosition.Length - temp; i++)
			{
				int count = 0;
				for (float j = 0; j < steps; j++)
				{
					if (count % 2 >= 1 || !isDotted)
						Debug.DrawLine(HermiteSpline(i, j), HermiteSpline(i, j + 1f), Palette.ColorWheel(i, 12));
					count++;
				}
			}
		}

	}


	void OnDrawGizmos()
	{
		if (keyPosition[0])
		{
			if (keyPosition.Length > 1)
			{
				for (int i = 0; i < rb.Count; i++)
				{
					Gizmos.color = Palette.ColorWheel(i, 12);
					Gizmos.DrawSphere(HermiteSpline(currentSegment[i], currentStep[i]), 1f);
				}
			}
		}
	}

	Vector3 HermiteSpline(int i, float currStep)
	{
        return new Vector3();
        /*/
		return
			SplineInterpolation.HermiteSpline(
			new Vector3[] {
                keyPosition[i % keyPosition.Length].position,
                keyPosition[(i + 1) % keyPosition.Length].position },
			new Vector3[] {
                keyPosition[i % keyPosition.Length].transform.forward,
                keyPosition[(i + 1) % keyPosition.Length].transform.forward},
			(int)currStep,
			(int)steps
			);
        //*/
	}


	void Loop(int i)
	{

		if (keyPosition.Length > 1)
		{
			// Calculate new position for object
			interpolatedPosition = HermiteSpline(currentSegment[i], currentStep[i]);

			//
			//go.transform.position = interpolatedPosition;
			if (rb[i])
				rb[i].MovePosition(interpolatedPosition);

			currentStep[i] += steps * Time.deltaTime / (lengthPerSegment[currentSegment[i]] / (pathLength / entireCycleTime));

			if (currentStep[i] >= steps)
			{
				// Some math here! Convert remainder of last segment into scaled % of new segment
				currentStep[i] %= steps;
				currentStep[i] /= lengthPerSegment[currentSegment[i]];
				currentStep[i] *= lengthPerSegment[(currentSegment[i] + 1) % keyPosition.Length];

				//debugTime = Time.time;
				currentSegment[i]++;
				currentSegment[i] %= keyPosition.Length;
			}
		}
		else
		{
			Debug.LogError("Not enough points in array!");
			Debug.Break();
		}
	}


	void Line(int i)
	{
		if (!hasFinished[i])
		{
			if (keyPosition.Length > 1 && currentSegment[i] != keyPosition.Length - 1)
			{
				// Calculate new position for object
				interpolatedPosition = HermiteSpline(currentSegment[i], currentStep[i]);

				//
				//go.transform.position = interpolatedPosition;
				if (rb[i])
					rb[i].MovePosition(interpolatedPosition);

				currentStep[i] += steps * Time.deltaTime / (lengthPerSegment[currentSegment[i]] / (pathLength / entireCycleTime));
				//currentStep += steps * Time.deltaTime * entireCycleTime / (float)keyPosition.Length;
				if (currentStep[i] >= steps)
				{
					// Some math here! Convert remainder of last segment into scaled % of new segment
					currentStep[i] = currentStep[i] % steps;
					currentStep[i] = currentStep[i] / lengthPerSegment[currentSegment[i]] * lengthPerSegment[(currentSegment[i] + 1) % keyPosition.Length];
					//Debug.Log(currentStep);

					//Debug.Log("Time: " + (Time.time) + " Pos: " + lengthPerSegment[currentSegment[i]]);
					//debugTime = Time.time;
					currentSegment[i] = (currentSegment[i] + 1) % keyPosition.Length;
				}

			}
			else
			{
				hasFinished[i] = true;
				OnEndLine(i);
			}
		}
	}


	void OnEndLine(int i)
	{
		if (rb[i])
			rb[i].velocity = keyPosition[keyPosition.Length - 1].transform.forward * pathLength / entireCycleTime;

		Destroy(rb[i], 5f);

		//EnemyFire fireController = rb[i].gameObject.GetComponent<EnemyFire>(); ;
		//if (fireController)
		//{
		//	fireController.enabled = false;
		//}
	}


	public void AddField(int insertPosition)
	{
		rb.Insert(insertPosition, null);
		currentSegment.Insert(insertPosition, 0);
		currentStep.Insert(insertPosition, 0f);
	}

	public void DeleteEntry(int deletePosition)
	{
		rb.RemoveAt(deletePosition);
		currentSegment.RemoveAt(deletePosition);
		currentStep.RemoveAt(deletePosition);
	}

}
