﻿using UnityEngine;
using System.Collections;

public class CameraFrame : MonoBehaviour
{
    public Transform[] transforms;
    Vector3 lookatTarget;
    [Range(0f, 1f)]
    public float ScaleFactor = 1f;

    void FixedUpdate()
    {
        lookatTarget = new Vector3();

        foreach (Transform t in transforms)
            lookatTarget += t.position;

        lookatTarget /= transforms.Length;
        lookatTarget *= ScaleFactor;

        transform.LookAt(lookatTarget);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;

        foreach (Transform t in transforms)
            Gizmos.DrawLine(t.position, lookatTarget);

        Gizmos.DrawLine(Camera.main.transform.position, lookatTarget);

        UnityEditor.Handles.DrawWireDisc(Vector3.zero, Vector3.up, GameSettings.radiusTotal * ScaleFactor);
    }

}
